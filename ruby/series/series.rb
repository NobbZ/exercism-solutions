class Series
  def initialize(num)
    @num = num.chars
  end

  def slices(width)
    fail ArgumentError if width > @num.length

    @num.tails
      .map    { |x| x.take width }
      .reject { |x| x.size < width }
      .map    { |x| x.join }
  end
end

module ArrayTailsHelper
  def tails
    (0..self.length).map do |idx|
      self[idx..-1]
    end
  end
end

class Array
  include ArrayTailsHelper
end
