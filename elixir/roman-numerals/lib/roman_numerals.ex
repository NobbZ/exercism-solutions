defmodule RomanNumerals do
  @arab_to_roman [
    {1000, "M"},
    {900, "CM"},
    {500, "D"},
    {400, "CD"},
    {100, "C"},
    {90, "XC"},
    {50, "L"},
    {40, "XL"},
    {10, "X"},
    {9, "IX"},
    {5, "V"},
    {4, "IV"},
    {1, "I"}
  ]

  @spec numeral(pos_integer) :: String.t()
  def numeral(number) do
    {result, 0} =
      Enum.reduce(@arab_to_roman, {"", number}, fn {arab, roman}, {acc, n} ->
        repeats = div(n, arab)
        next_n = rem(n, arab)
        acc = acc <> String.duplicate(roman, repeats)
        {acc, next_n}
      end)

    result
  end
end
