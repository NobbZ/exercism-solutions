-module(sieve).

-export([sieve/1, test_version/0]).

sieve(N) ->
  Numbers = lists:seq(2, N),
  Primes = sieve(Numbers, [], N),
  lists:reverse(Primes).

test_version() -> 1.

sieve([], Primes, _Upper) ->
  Primes;
sieve([N|Numbers], Primes, Upper) ->
  Multiples = lists:seq(2 * N, Upper, N),
  Numbers0 = Numbers -- Multiples,
  sieve(Numbers0, [N|Primes], Upper).