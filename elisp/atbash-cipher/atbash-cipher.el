;;; atbash-cipher.el --- Atbash-Cipher (exercism)

;;; Commentary:

;;; Code:

(require 'seq)
(require 'subr-x)

(defun encode (plaintext)
  "Encode PLAINTEXT to atbash-cipher encoding."
  (let* ((lowercased (downcase plaintext))
         (stripped (seq-filter #'atbash--alnump
                               lowercased))
         (coded (mapcar #'atbash--code-char stripped))
         (grouped (atbash--group coded)))
    grouped
  ))

(defun atbash--alnump (c)
  "Determine if C represents a digit or a lowercase letter in ASCII."
  (or (atbash--digitp c)
      (atbash--alphap c)))

(defun atbash--digitp (c)
  "Determine if C represents a digit in ASCII."
  (and (<= ?0 c)
       (<= c ?9)))

(defun atbash--alphap (c)
  "Determine if C represents a lowercase letter in ASCII."
  (and (<= ?a c)
       (<= c ?z)))

(defun atbash--code-char (c)
  "Transform a single char C to its encrypted form."
  (cond
   ((atbash--alphap c) (+ ?z (- ?a c)))
   (t c)))

(defun atbash--group (sequence)
  "Insert a space after every 5 items in SEQUENCE."
  (string-trim-left
   (concat
    (mapcan (lambda (group) `(?\s . ,group))
            (seq-partition sequence 5)))))

(provide 'atbash-cipher)
;;; atbash-cipher.el ends here
