#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
    local left=${1:-}
    local right=${2:-}

    local result=0

    if (( $# != 2 )); then
        printf "Usage: %s <%s> <%s>\n" hamming.sh strand1 strand2
        exit 1;
    fi

    if (( ${#left} != ${#right} )); then
        printf "left and right strands must be of equal length\n"
        exit 1;
    fi

    for (( i=0; i < ${#left}; i++ )); do
        if [[ ${left:$i:1} != ${right:$i:1} ]]; then ((result+=1)); fi
    done

    printf "%u\n" ${result}
}

main "$@"
