defmodule RNATranscription do
  @translation_table %{?A => ?U, ?C => ?G, ?G => ?C, ?T => ?A}

  def to_rna(dna) when is_list(dna), do: Enum.map(dna, &@translation_table[&1])
end
