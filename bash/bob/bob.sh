#!/usr/bin/env bash

set -o errexit
set -o nounset

# set -x

shout() {
  local all_caps=$(printf "%s" "$1" | tr "[a-z]" "[A-Z]")
  local all_low=$(printf "%s" "$1" | tr "[A-Z]" "[a-z]")

  [ "$all_caps" = "$1" -a ! "$all_low" = "$1" ]
}

question() {
  local len=$((${#1} - 1))
  local last="${1:$len:1}"
  
  [ "${last}" = "?" ]
}

silence() {
  [ -z ${1} ]
}

main() {
  local whitespace=" \\\\t\\\\n\\\\r"
  local sentence=$(printf "%s" ${1:-} | sed -e "s|^[${whitespace}]*||" -e "s|[${whitespace}]*$||")

  local response=""

  if shout "${sentence}" && question "${sentence}"; then
    response="Calm down, I know what I'm doing!"
  elif shout "${sentence}"; then
    response="Whoa, chill out!"
  elif question "${sentence}"; then
    response="Sure."
  elif silence "${sentence}"; then
    response="Fine. Be that way!"
  else
    response="Whatever."
  fi

  printf "%s\n" "${response}"
}

main "$@"
