﻿module Bob

open System

let active (b: bool) (pattern: 'a): 'a option =
    if b then Some(pattern)
    else None

let (|Shout|_|) (input: string) =
    active (input.ToUpper() = input && input.ToLower() <> input) Shout

let (|Question|_|) (input: string) =
    active (input.EndsWith("?")) Question

let (|Silence|_|) (input: string) =
    active (input = "") Silence

let response (input: string): string =
    let sentence = input.Trim()
    match sentence with
    | Shout & Question -> "Calm down, I know what I'm doing!"
    | Shout -> "Whoa, chill out!"
    | Question -> "Sure."
    | Silence -> "Fine. Be that way!"
    | _ -> "Whatever."
