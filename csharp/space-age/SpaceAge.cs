using System;

public class SpaceAge
{
    private static readonly double SECONDS_IN_EARTH_YEAR = 31557600.0;

    private static readonly double MERCURY_PERIOD = 0.2408467;
    private static readonly double VENUS_PERIOD = 0.61519726;
    private static readonly double MARS_PERIOD = 1.8808158;
    private static readonly double JUPITER_PERIOD = 11.862615;
    private static readonly double SATURN_PERIOD = 29.447498;
    private static readonly double URANUS_PERIOD = 84.016846;
    private static readonly double NEPTUNE_PERIOD = 164.79132;

    private readonly int seconds;
    public SpaceAge(int seconds) {
        this.seconds = seconds;
    }

    public double OnEarth() {
        return this.seconds / SpaceAge.SECONDS_IN_EARTH_YEAR;
    }

    public double OnMercury() {
        return this.ApplyPeriod(SpaceAge.MERCURY_PERIOD);
    }

    public double OnVenus() {
        return this.ApplyPeriod(SpaceAge.VENUS_PERIOD);
    }

    public double OnMars() {
        return this.ApplyPeriod(SpaceAge.MARS_PERIOD);
    }

    public double OnJupiter() {
        return this.ApplyPeriod(SpaceAge.JUPITER_PERIOD);
    }

    public double OnSaturn() {
        return this.ApplyPeriod(SpaceAge.SATURN_PERIOD);
    }

    public double OnUranus() {
        return this.ApplyPeriod(SpaceAge.URANUS_PERIOD);
    }

    public double OnNeptune() {
        return this.ApplyPeriod(SpaceAge.NEPTUNE_PERIOD);
    }

    private double ApplyPeriod(double period) {
        return this.OnEarth() / period;
    }
}
