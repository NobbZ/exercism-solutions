#ifndef HELLO_WORLD_H
#define HELLO_WORLD_H

#include <string>

namespace hello_world {
  std::string hello();
}

#endif // HELLO_WORLD_H
