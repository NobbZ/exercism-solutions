#ifndef TRINARY_TRINARY_H
#define TRINARY_TRINARY_H

#include <string>

namespace trinary {
  unsigned int to_decimal(std::string const &tern_nmbr);
}

#endif //TRINARY_TRINARY_H
