module School (School, add, empty, grade, sorted) where

import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

newtype School = School { unSchool :: Map Int [String]}

add :: Int -> String -> School -> School
add gradeNum student = School . M.insertWith (++) gradeNum [student] . unSchool

empty :: School
empty = School M.empty

grade :: Int -> School -> [String]
grade gradeNum = sort . M.findWithDefault [] gradeNum . unSchool

sorted :: School -> [(Int, [String])]
sorted = sort . M.toList . M.map sort . unSchool
