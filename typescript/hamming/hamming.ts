export function compute(l: string, r: string): number {
    if (l.length !== r.length) {
        throw "DNA strands must be of equal length."
    }

    let result = 0

    for (let i = 0; i < l.length; i++) {
        if (l[i] !== r[i]) {
            result++
        }
    }

    return result
}