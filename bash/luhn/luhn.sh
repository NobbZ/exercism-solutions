#!/usr/bin/env bash

set -o errexit
set -o nounset

println() {
  printf "%s\n" $1
}

print_invalid() {
  println "false"
}

print_valid() {
  println "true"
}

only_digits() {
  [[ ${1} =~ ^[0-9]+$ ]]
}

double_every_other() {
  local input=${1}
  local output=${input}

  for (( i=${#input} - 2; i >= 0; i-=2 )); do
    local n=${1:${i}:1}
    n=$(( 2 * ${n} ))
    if (( n > 9 )); then
      n=$(( ${n} - 9 ))
    fi
    output=$(echo ${output} | sed s/./${n}/$((i + 1)))
  done

  printf "%s" ${output}
}

main() {
  local number=$( echo ${1:-} | sed -r 's|\s||g' )
  local modulus=0

  if ( ! only_digits "${number}" ) || (( ${#number} <= 1 )); then
    print_invalid
    exit 0
  fi

  number=$(double_every_other ${number})

  for (( i=0; i < ${#number}; i++ )); do
    modulus=$(( ${modulus} + ${number:${i}:1} ))
    if (( ${modulus} >= 10 )); then modulus=$(( ${modulus} - 10 )); fi
  done

  if (( ${modulus} == 0 )); then
    print_valid
  else
    print_invalid
  fi
}

main "$@"

# Local Variables:
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
