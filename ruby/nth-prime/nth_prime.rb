module Prime
  def self.nth(n)
    fail ArgumentError if n <= 0

    @@primes ||= [2, 3]
    @@wheel ||= 1

    @@primes[n - 1] || update_primes_until(n)
  end

  private

  def self.update_primes_until(n)
    loop do
      candidate_1 = 6 * @@wheel - 1
      candidate_2 = 6 * @@wheel + 1
      @@primes << candidate_1 if is_prime? candidate_1
      @@primes << candidate_2 if is_prime? candidate_2
      @@wheel += 1
      break if @@primes.size >= n
    end
    @@primes[n-1]
  end

  def self.is_prime?(n)
    @@primes.each { |p|
      return true if p == n
      return true if p * p > n
      return false if n % p == 0
    }
  end
end

module BookKeeping
  VERSION = 1
end