class Phrase < String
  def initialize(str)
    super(str)
  end

  def word_count
    @count ||= do_count
  end

  private

  def do_count
    self.scan(/[[:alnum:]](?:[[:alnum:]']*[[:alnum:]])?/)
      .map { |w| w.downcase }
      .reduce(Hash.new(0)) do |wc, w|
        wc[w] += 1
        wc
      end
  end
end

module BookKeeping
  VERSION = 1
end