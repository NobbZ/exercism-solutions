{
  callPackage,
  stdenv,
  self,
  lib,
  nodePackages,
}: let
  runExercise = slug: let
    ename = builtins.replaceStrings ["-"] ["_"] slug;

    modules = (callPackage (./. + "/${slug}/default.nix") {}).shell.nodeDependencies;
  in
    stdenv.mkDerivation rec {
      pname = "exercism_javascript_${ename}";
      version = "g${self.shortRev or "dirty"}";

      phases = ["unpackPhase" "patchPhase" "configPhase" "checkPhase"];

      src = ./. + "/${slug}";

      configPhase = ''
        # chmod +xrw cache
        # export HOME=$(pwd)/.home
        # mkdir -p $HOME

        ln -s ${modules}/lib/node_modules node_modules
      '';

      doCheck = true;
      checkPhase = ''
        ${nodePackages.npm}/bin/npm run test | tee $out
      '';
    };
in
  lib.attrsets.genAttrs [
    "amusement-park"
    "annalyns-infiltration"
    "coordinate-transformation"
    "poetry-club-door-policy"
    "factory-sensors"
    "lucky-numbers"
    "lasagna"
    "amusement-park"
    "fruit-picker"
    "elyses-destructured-enchantments"
    "high-score-board"
    "elyses-enchantments"
    "hello-world"
    "mixed-juices"
    "bird-watcher"
    "vehicle-purchase"
    "freelancer-rates"
  ]
  runExercise
#   "amusement-park" = runExercise "amusement-park";
#   "annalyns-infiltration" = runExercise "annalyns-infiltration";
# }

