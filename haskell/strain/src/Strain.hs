module Strain (keep, discard) where

keep :: (a -> Bool) -> [a] -> [a]
keep p = foldr (\ y ys -> if p y then y:ys else ys) []

discard :: (a -> Bool) -> [a] -> [a]
discard p = keep (not . p)
