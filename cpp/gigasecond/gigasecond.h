#ifndef GIGASECOND_GIGASECOND_H
#define GIGASECOND_GIGASECOND_H

#include <boost/date_time/posix_time/posix_time.hpp>

namespace gigasecond {
  boost::posix_time::ptime advance(boost::posix_time::ptime const &);
}

#endif //GIGASECOND_GIGASECOND_H
