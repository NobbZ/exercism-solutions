defmodule KitchenCalculator do
  def get_volume({_, n}), do: n

  def to_milliliter({:milliliter, _} = t), do: t
  def to_milliliter({:cup, n}), do: {:milliliter, 240 * n}
  def to_milliliter({:fluid_ounce, n}), do: {:milliliter, 30 * n}
  def to_milliliter({:teaspoon, n}), do: {:milliliter, 5 * n}
  def to_milliliter({:tablespoon, n}), do: {:milliliter, 15 * n}

  def from_milliliter({:milliliter, _} = t, :milliliter), do: t
  def from_milliliter({:milliliter, n}, :cup), do: {:cup, n / 240}
  def from_milliliter({:milliliter, n}, :fluid_ounce), do: {:fluid_ounce, n / 30}
  def from_milliliter({:milliliter, n}, :teaspoon), do: {:teaspoon, n / 5}
  def from_milliliter({:milliliter, n}, :tablespoon), do: {:tablespoon, n / 15}

  def convert(volume_pair, unit) do
    volume_pair |> to_milliliter() |> from_milliliter(unit)
  end
end
