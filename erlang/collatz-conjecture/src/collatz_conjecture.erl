-module(collatz_conjecture).

-export([steps/1, test_version/0]).


steps(N) when is_integer(N), N > 0 ->
    steps(N, 0);
steps(N) when is_integer(N), N =< 0 ->
    {error, "Only positive numbers are allowed"}.


steps(1, X) ->
    X;
steps(N, X) when N rem 2 =:= 0 ->
    steps(N div 2, X + 1);
steps(N, X) when N rem 2 =/= 0 ->
    steps(N * 3 + 1, X + 1).



test_version() -> 2.
