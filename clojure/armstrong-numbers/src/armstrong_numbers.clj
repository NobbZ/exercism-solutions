(ns armstrong-numbers
  (:require [clojure.core :refer [println]]))

(defn digits [num]
  {:pre [(integer? num)]}
  (->> num
       (iterate #(quot % 10)) ; divide by 10, iteratively
       (take-while pos?)      ; stop when result is zero
       (mapv #(mod % 10))     ; get modulo of each number
       rseq))                 ; reverse to get original order

(defn exp [a b]
  {:pre [(integer? a)
         (integer? b)]}
  (reduce * (repeat b a)))

(defn armstrong? [num]
  {:pre [(integer? num)]}
  (let [digs (digits num)   ; list of digits
        len (count digs)]   ; cound of digits
    (->> digs
         (map #(exp % len)) ; each digit to the power of the digit count
         (reduce +)         ; summing them up
         (= num))))         ; compare result to number
