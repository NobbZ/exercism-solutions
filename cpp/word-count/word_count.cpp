#include "word_count.h"

#include <algorithm>
#include <regex>

word_count::histogram word_count::words(std::string const text) {
  // a map holding the result set
  histogram result = histogram{};

  // normalize the string by lowercasing it...
  std::string normalized {text};
  std::transform(std::cbegin(text), std::cend(text), std::begin(normalized), ::tolower);

  // Preparing word-wise iterator
  std::regex const     re {"[[:alnum:]](?:[[:alnum:]']*[[:alnum:]])?"};
  std::sregex_iterator re_iterator {std::cbegin(normalized), std::cend(normalized), re};

  // accumulate the result
  std::for_each(re_iterator, std::sregex_iterator{}, [&result](std::smatch const &m) {
    ++result[m.str()];
  });

  return result;
}
