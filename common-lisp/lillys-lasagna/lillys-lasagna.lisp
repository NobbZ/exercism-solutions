(defpackage :lillys-lasagna
  (:use :cl)
  (:export :expected-time-in-oven
           :remaining-minutes-in-oven
           :preparation-time-in-minutes
           :elapsed-time-in-minutes))

(in-package :lillys-lasagna)

;; Define function expected-time-in-oven
(defun expected-time-in-oven ()
  "Returns the expected time in the oven"
  337)

;; Define function remaining-minutes-in-oven
(defun remaining-minutes-in-oven (minutes)
  "How many minutes do remain?"
  (- (expected-time-in-oven) minutes))

;; Define function preparation-time-in-minutes
(defun preparation-time-in-minutes (layer-count)
  "How long do we need to prepare all the layers"
  (* layer-count 19))

;; Define function elapsed-time-in-minutes
(defun elapsed-time-in-minutes (layer-count minutes-in-oven)
  "how much time has passed already?"
  (+ (preparation-time-in-minutes layer-count) minutes-in-oven))