(ns rna-transcription)

(defn convert
  "Converts a single DNA-nucleotide to its RNA counterpart"
  [^Character nuc]
  {:post [%]}
  (case nuc
    \G \C
    \C \G
    \T \A
    \A \U
    false))

(defn to-rna
  "Converts a DNA-sequence to an RNA sequence."
  [^String dna]
  (apply str (map convert dna)))
