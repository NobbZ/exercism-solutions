#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
  local n=${1}
  local result=0

  if (( $n < 1 )); then
    printf "Error: Only positive numbers are allowed"
    exit 1
  fi

  while (( $n != 1 )); do
    result=$(( $result + 1 ))

    if (( n % 2 == 0 )); then
      n=$(( $n / 2 ))
    else
      n=$(( $n * 3 + 1 ))
    fi
  done

  printf "%d\n" $result
}

main "$@"

# Local Variables:
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
