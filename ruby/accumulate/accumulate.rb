module Accumulate
  def accumulate(&proc)
    self.reduce(Array.new) do |acc, x|
      acc << proc.yield(x)
    end
  end
end

class Array
  include Accumulate
end

module BookKeeping
  VERSION = 1
end