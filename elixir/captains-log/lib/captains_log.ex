defmodule CaptainsLog do
  @planetary_classes ["D", "H", "J", "K", "L", "M", "N", "R", "T", "Y"]

  def random_planet_class() do
    Enum.random(@planetary_classes)
  end

  def random_ship_registry_number() do
    number =
      1..4 |> Enum.map(fn _ -> 1..9 |> Enum.random() |> Integer.to_string() end) |> Enum.join()

    "NCC-#{number}"
  end

  def random_stardate() do
    :rand.uniform() * (42000.0 - 41000.0) + 41000.0
  end

  def format_stardate(stardate) do
    stardate |> then(&:io_lib.format("~.1f", [&1])) |> List.to_string()
  end
end
