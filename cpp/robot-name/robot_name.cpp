#include "robot_name.h"

#include <random>

static std::random_device                  rd;
static std::default_random_engine          rnd_eng {rd()};
static std::uniform_int_distribution<char> letter_generator {'A', 'Z'};
static std::uniform_int_distribution<char> number_generator {'0', '9'};

inline char l() {
  return letter_generator(rnd_eng);
}

inline char d() {
  return number_generator(rnd_eng);
}

std::string gen_name() {
  return std::string {l(), l(), d(), d(), d()};
}

robot_name::robot::robot() : _name(gen_name()) {};

robot_name::robot& robot_name::robot::reset() {
  this->_name = gen_name();
  return *this;
}

std::string robot_name::robot::name() const {
  return this->_name;
}