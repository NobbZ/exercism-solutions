# Use the Plot struct as it is provided
defmodule Plot do
  @enforce_keys [:plot_id, :registered_to]
  defstruct [:plot_id, :registered_to]
end

defmodule CommunityGarden do
  def start(opts \\ []), do: Agent.start(fn -> [] end, opts)

  def list_registrations(pid), do: Agent.get(pid, &Function.identity/1)

  def register(pid, register_to) do
    id =
      case list_registrations(pid) do
        [] -> 1
        [%{plot_id: old_id} | _] -> old_id + 1
      end

    plot = %Plot{plot_id: id, registered_to: register_to}

    Agent.update(pid, &[plot | &1])

    plot
  end

  def release(pid, plot_id) do
    Agent.update(pid, &Enum.reject(&1, fn p -> p.plot_id == plot_id end))
  end

  def get_registration(pid, plot_id) do
    list_registrations(pid)
    |> Enum.find({:not_found, "plot is unregistered"}, &(&1.plot_id == plot_id))
  end
end
