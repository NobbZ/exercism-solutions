#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
  local strand=${1}

  local -A nucs=(
    [A]=0
    [C]=0
    [G]=0
    [T]=0
  )

  for ((i=0; i<${#strand}; i++)); do
    nuc=${strand:$i:1}

    case $nuc in
      (A|C|G|T)
        nucs[$nuc]=$(( nucs[${nuc}] + 1 ));;
      (*)
        printf "Invalid nucleotide in strand"
        exit 1;;
    esac
  done

  printf "A: %d\nC: %d\nG: %d\nT: %d\n" ${nucs[A]} ${nucs[C]} ${nucs[G]} ${nucs[T]}
}

main "$@"

# Local Variables:
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
