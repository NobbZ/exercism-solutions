defmodule Username do
  def sanitize(username) do
    username
    |> Enum.flat_map(
      &case &1 do
        ?ä -> 'ae'
        ?ü -> 'ue'
        ?ö -> 'oe'
        ?ß -> 'ss'
        c -> [c]
      end
    )
    |> Enum.filter(&(&1 in ?a..?z or &1 in '_'))
  end
end
