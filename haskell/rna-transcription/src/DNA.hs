module DNA (toRNA) where

toRNA :: String -> Maybe String
toRNA = foldr rewrap (Just []) . map convertNuc
  where
    convertNuc c = case c of
      'G' -> Just 'C'
      'C' -> Just 'G'
      'T' -> Just 'A'
      'A' -> Just 'U'
      _ -> Nothing
    rewrap _ Nothing = Nothing
    rewrap Nothing _ = Nothing
    rewrap (Just x) (Just ys) = Just (x:ys)
