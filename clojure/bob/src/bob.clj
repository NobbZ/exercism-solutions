(ns bob
  (:require [clojure.string :as str]))

(defn- silent?
  "Checks if a given sentence is only silence." 
  [s]
  (str/blank? s))

(defn- shout?
  "Checks if a given sentence is shout."
  [s]
  (and (=    (str/upper-case s) s)
       (not= (str/lower-case s) s)))

(defn- question? 
  "Checks if a given sentence is a question."
  [s]
  (.endsWith s "?"))

(defn- forcefully-asked?
  "Checks if a given sentence is forcefully asked."
  [s]
  (and (shout?    s)
       (question? s)))

(defn response-for
  "Returns Bobs answer when you talk to him."
  [s]
  (let [trimmed (str/trim s)]
    (condp #(%1 %2) trimmed
      forcefully-asked? "Calm down, I know what I'm doing!"
      silent?           "Fine. Be that way!"
      shout?            "Whoa, chill out!"
      question?         "Sure."
      "Whatever.")))
