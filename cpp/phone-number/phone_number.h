#ifndef PHONE_NUMBER_PHONE_NUMBER_H
#define PHONE_NUMBER_PHONE_NUMBER_H

#include <string>

namespace phone {
  class phone_number {
  public:
    phone_number(std::string);
    std::string number() const;
    std::string area_code() const;

    operator std::string() const;

  private:
    const std::string number_str;

    std::string mid_part() const;
    std::string last_part() const;
  };
}

typedef phone::phone_number phone_number;

#endif //PHONE_NUMBER_PHONE_NUMBER_H
