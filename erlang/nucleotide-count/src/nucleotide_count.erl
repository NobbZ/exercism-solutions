-module(nucleotide_count).

-export([count/2, nucleotide_counts/1, test_version/0]).

-export_type([result/2]).

-type result(A, B) :: A | {error, B}.

-spec(count(string:string(), string:string()) -> result(non_neg_integer(), string:string())).
count(DNA, [Nuc]) when (Nuc =:= $A); (Nuc =:= $C); (Nuc =:= $G); (Nuc =:= $T) ->
  Counter = fun
    (X, Acc) when X =:= Nuc -> Acc + 1;
    (_, Acc) -> Acc
  end,
  lists:foldr(Counter, 0, DNA);
count(_, _) -> erlang:error("Invalid nucleotide").

nucleotide_counts(DNA) when is_list(DNA) ->
  Initial = #{"A" => 0, "C" => 0, "G" => 0, "T" => 0},
  Counter = fun
    (X, Acc) -> maps:update([X], maps:get([X], Acc) + 1, Acc)
  end,
  #{"A" := A, "C" := C, "G" := G, "T" := T} = lists:foldr(Counter, Initial, DNA),
  [{"A", A},  {"T", T}, {"C", C}, {"G", G}].

test_version() -> 1.