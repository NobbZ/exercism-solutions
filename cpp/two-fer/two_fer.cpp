#include "two_fer.h"

#include <sstream>

std::string two_fer::two_fer(std::string const & s) {
  std::ostringstream os;

  os << "One for " << s << ", one for me.";

  return os.str();
}
