defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t) :: map()
  def count(sentence) do
    ~r/[[:alnum:]](?:[[:alnum:]'-]*[[:alnum:]])?/u
    |> Regex.scan(sentence)
    |> Stream.transform(nil, fn (l, nil) when is_list(l) -> {l, nil} end)
    |> Stream.map(&String.downcase/1)
    |> Enum.reduce(%{}, fn (word, acc) -> Map.update(acc, word, 1, &(&1 + 1)) end)
  end
end