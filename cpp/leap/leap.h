#ifndef LEAP_LEAP_H
#define LEAP_LEAP_H

namespace leap {
  inline bool is_leap_year(int year) {
    return !(year % 100) ? !(year % 400) : !(year % 4);
  }
}

#endif //LEAP_LEAP_H
