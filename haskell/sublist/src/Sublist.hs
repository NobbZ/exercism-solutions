module Sublist (Sublist(..), sublist) where

import           Data.Function (on)
import           Data.List     (isInfixOf)

data Sublist = Equal | Sublist | Superlist | Unequal deriving (Eq, Show)

sublist :: Eq a => [a] -> [a] -> Sublist
sublist xs ys = let compareLengths = compare `on` length in
  case compareLengths xs ys of
    EQ | xs == ys          -> Equal
    LT | xs `isInfixOf` ys -> Sublist
    GT | ys `isInfixOf` xs -> Superlist
    _                      -> Unequal
