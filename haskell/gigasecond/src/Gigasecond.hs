module Gigasecond (fromDay) where

import           Data.Time.Clock (UTCTime, addUTCTime)

fromDay :: UTCTime -> UTCTime
fromDay = let gigasecond = 10^(9 :: Int) in
  addUTCTime gigasecond
