defmodule RemoteControlCar do
  @enforce_keys [:nickname]
  defstruct [:nickname, battery_percentage: 100, distance_driven_in_meters: 0]

  def new(), do: %__MODULE__{nickname: "none"}

  def new(nickname), do: %__MODULE__{nickname: nickname}

  def display_distance(%__MODULE__{distance_driven_in_meters: dist}), do: "#{dist} meters"

  def display_battery(%__MODULE__{battery_percentage: 0}), do: "Battery empty"
  def display_battery(%__MODULE__{battery_percentage: bat}), do: "Battery at #{bat}%"

  def drive(%__MODULE__{battery_percentage: 0} = car), do: car

  def drive(%__MODULE__{battery_percentage: bat, distance_driven_in_meters: m} = car) do
    %{car | battery_percentage: bat - 1, distance_driven_in_meters: m + 20}
  end
end
