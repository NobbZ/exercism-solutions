module ETL (transform) where

import           Data.Char (toLower)
import           Data.Map.Strict (Map, fromList, toList)

transform :: Map a String -> Map Char a
transform = fromList . flatmap f . toList
  where
    flatmap = flip (>>=)
    f (score, letters) = zip (map toLower letters) $ repeat score