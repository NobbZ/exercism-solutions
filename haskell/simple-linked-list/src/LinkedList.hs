module LinkedList (LinkedList, datum, fromList, isNil, new, next, nil, reverseLinkedList, toList) where

data LinkedList a = Nil | a :++: (LinkedList a) deriving (Eq, Show)

instance Foldable LinkedList where
  foldr _f acc Nil       = acc
  foldr f  acc (x:++:xs) = x `f` foldr f acc xs

-- constructing
-- ------------

nil :: LinkedList a
nil = Nil

new :: a -> LinkedList a -> LinkedList a
new = (:++:)

-- destructuring
-- -------------

datum :: LinkedList a -> a
datum Nil      = error "Empty LinkedList"
datum (x:++:_) = x

next :: LinkedList a -> LinkedList a
next Nil       = error "Empty LinkedList"
next (_:++:xs) = xs

-- predicates
-- ----------

isNil :: LinkedList a -> Bool
isNil Nil = True
isNil _   = False

-- transformation
-- --------------

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList = foldl (flip (:++:)) Nil

-- conversion
-- ----------

fromList :: [a] -> LinkedList a
fromList = foldr (:++:) Nil

toList :: LinkedList a -> [a]
toList = foldr (:) []
