defmodule FreelancerRates do
  @hours_per_month 8 * 22

  def daily_rate(hourly_rate) do
    8.0 * hourly_rate
  end

  def apply_discount(before_discount, discount) do
    before_discount * (1 - discount / 100)
  end

  def monthly_rate(hourly_rate, discount) do
    ceil(apply_discount(@hours_per_month * hourly_rate, discount))
  end

  def days_in_budget(budget, hourly_rate, discount) do
    floor(budget / apply_discount(daily_rate(hourly_rate), discount) * 10) / 10
  end
end
