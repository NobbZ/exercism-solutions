open! Base

let transform old =
  let single_transform (score, cs) =
    List.map cs ~f:(fun c -> (Char.lowercase c, score)) in
  let compare (a, _) (b, _) =
    Char.compare a b in
  old
  |> List.concat_map ~f:single_transform
  |> List.sort ~compare:compare
