#include "sum_of_multiples.h"

#include <algorithm>

bool divisible_by(int x, int divisor) {
  return !(x % divisor);
}

bool divisible_by_one_of(int x, sum_of_multiples::divisor_list divisors) {
  return std::any_of(divisors.cbegin(), divisors.cend(), [x](int divisor) {
    return divisible_by(x, divisor);
  });
}

int sum_of_multiples::to(sum_of_multiples::divisor_list divisors, int max) {
  int result {0};

  for (int i = 1; i < max; ++i) {
    if (divisible_by_one_of(i, divisors)) { result += i; }
  }

  return result;
}
