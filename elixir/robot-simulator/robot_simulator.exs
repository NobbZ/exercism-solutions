defmodule RobotSimulator do
  @directions [:north, :east, :south, :west]

  defstruct [:d, :p]

  defguardp is_direction(dir) when dir in @directions

  defguardp is_position(pos)
            when is_tuple(pos) and
                   tuple_size(pos) === 2 and
                   pos |> elem(0) |> is_integer and
                   pos |> elem(1) |> is_integer()

  @spec create(direction :: atom, position :: {integer, integer}) :: any
  def create(direction \\ :north, position \\ {0, 0})
  def create(d, p) when is_direction(d) and is_position(p), do: %__MODULE__{d: d, p: p}
  def create(d, _p) when not is_direction(d), do: {:error, "invalid direction"}
  def create(_direction, _position), do: {:error, "invalid position"}

  @spec simulate(robot :: any, instructions :: String.t()) :: any
  def simulate(robot, ""), do: robot
  def simulate(robot, <<?L, rest::binary>>), do: robot |> left() |> simulate(rest)
  def simulate(robot, <<?R, rest::binary>>), do: robot |> right() |> simulate(rest)
  def simulate(robot, <<?A, rest::binary>>), do: robot |> advance() |> simulate(rest)
  def simulate(_robot, _instructions), do: {:error, "invalid instruction"}

  @spec direction(robot :: any) :: atom
  def direction(%__MODULE__{d: d}), do: d

  @spec position(robot :: any) :: {integer, integer}
  def position(%__MODULE__{p: p}), do: p

  @clockwise @directions
             |> Stream.cycle()
             |> Enum.take(5)
             |> Enum.chunk_every(2, 1, :discard)

  @clockwise
  |> Enum.each(fn [orig, dest] ->
    defp right(%__MODULE__{d: unquote(orig)} = r), do: %{r | d: unquote(dest)}
  end)

  @clockwise
  |> Enum.each(fn [dest, orig] ->
    defp left(%__MODULE__{d: unquote(orig)} = r), do: %{r | d: unquote(dest)}
  end)

  defp advance(%__MODULE__{d: :north, p: {x, y}} = r), do: %{r | p: {x, y + 1}}
  defp advance(%__MODULE__{d: :south, p: {x, y}} = r), do: %{r | p: {x, y - 1}}
  defp advance(%__MODULE__{d: :west, p: {x, y}} = r), do: %{r | p: {x - 1, y}}
  defp advance(%__MODULE__{d: :east, p: {x, y}} = r), do: %{r | p: {x + 1, y}}
end
