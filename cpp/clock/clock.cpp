#include "clock.h"

#include <sstream>

static const int MINUTES_A_DAY {60 * 24};

template<typename T>
  inline T mod(T n, T m) {
    return (n % m + m) % m;
  }

date_independent::clock::clock(int minutes) : _minutes(mod(minutes, MINUTES_A_DAY)) {}
date_independent::clock::clock(int hours, int minutes) : _minutes(mod(hours * 60 + minutes, MINUTES_A_DAY)) {}

date_independent::clock date_independent::clock::at(int hours, int minutes) {
  return clock(hours, minutes);
}

date_independent::clock date_independent::clock::plus(int minutes) {
  return clock(this->_minutes + minutes);
}

date_independent::clock date_independent::clock::minus(int minutes) {
  return this->plus(-minutes);
}

date_independent::clock::operator std::string() const {
  int hours = this->_minutes / 60;
  int mins  = mod(this->_minutes, 60);
  return ((hours >= 10) ? "" : "0") + std::to_string(hours) + ":"
       + ((mins  >= 10) ? "" : "0") + std::to_string(mins);
}

bool date_independent::clock::operator==(date_independent::clock other) const {
  return this->_minutes == other._minutes;
}

bool date_independent::clock::operator!=(date_independent::clock other) const {
  return ! (*this == other);
}
