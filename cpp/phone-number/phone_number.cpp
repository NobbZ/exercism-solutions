#include "phone_number.h"

#include <numeric>
#include <sstream>

static const std::string DEFAULT_NUMBER {"0000000000"};

std::string clean(std::string string) {
  std::string result;

  result = std::accumulate(string.cbegin(), string.cend(), std::string{}, [](std::string acc, char c) {
    if (isdigit(c)) { acc.push_back(c); }
    return acc;
  });

  switch (result.length()) {
  case 10:
    return result;
  case 11:
    if (result[0] == '1') {
      return result.erase(0, 1);
    }
    // fall through to default case!
  default:
    return DEFAULT_NUMBER;
  }
}

phone_number::phone_number(std::string string) : number_str(clean(string)) { }

std::string phone_number::number() const {
  return this->number_str;
}

std::string phone_number::area_code() const {
  return this->number_str.substr(0, 3);
}

phone_number::operator std::string() const {
  return "(" + this->area_code() + ") "
         + this->mid_part() + "-"
         + this->last_part();
}

std::string phone_number::mid_part() const {
  return this->number_str.substr(3, 3);
}

std::string phone_number::last_part() const {
  return this->number_str.substr(6, 4);
}
