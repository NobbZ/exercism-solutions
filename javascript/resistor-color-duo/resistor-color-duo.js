// @ts-check

/** @type {string[]} */
const COLORS = ["black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"];

/**
 * 
 * @param {string} band 
 * @returns number
 */
const decodeBand = (band) => COLORS.indexOf(band);

/**
 * @param {string[]} bands
 * 
 * @returns {number}
 */
export const decodedValue = ([bandA, bandB]) => {
  return decodeBand(bandA) * 10 + decodeBand(bandB)
};
