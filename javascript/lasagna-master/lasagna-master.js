/// <reference path="./global.d.ts" />
// @ts-check

/** @typedef {Record<string, number>} Recipe */

/** 
 * @param remainingTime {number}
 * @returns {string}
 */
export function cookingStatus(remainingTime) {
    if (remainingTime === undefined) {
        return "You forgot to set the timer.";
    }

    if (remainingTime === 0) {
        return 'Lasagna is done.';
    }

    return 'Not done, please wait.';
}

/** 
 * @param {string[]} layers 
 * @param {number} minutesPerLayer 
 * @returns {number}
 */
export function preparationTime(layers, minutesPerLayer = 2) {
    return layers.length * minutesPerLayer;
}

/**
 * @param {string[]} layers
 * @returns {Quantities}
 */
export function quantities(layers) {
    let result = { noodles: 0, sauce: 0 };

    for (let layer of layers) {
        switch (layer) {
            case 'sauce':
                result.sauce += 0.2;
                break;
        
            case 'noodles':
                result.noodles += 50;
                break;
        }
    }

    return result;
}

/**
 * @param {string[]} friendsList 
 * @param {string[]} myList 
 */
export function addSecretIngredient(friendsList, myList) {
    myList.push(friendsList[friendsList.length - 1])
}

/**
 * @param {Recipe} recipe
 * @param {number} portions
 * @returns {Recipe}
 */
export function scaleRecipe(recipe, portions) {
    let result = {};

    for (let item in recipe) {
        result[item] = recipe[item] * portions / 2;
    }

    return result;
}