#lang racket

(provide (contract-out
          [perfect-numbers (-> integer? integer-list?)]))

(require math/number-theory
         math/base)

(define (perfect-numbers n)
  (filter perfect-number? (range 1 (add1 n))))

(define (perfect-number? n)
  (= (sum (divisors n)) (* 2 n)))

(define (integer-list? ns)
  (and (list? ns) (andmap integer? ns)))
