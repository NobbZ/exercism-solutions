#include "collatz_conjecture.h"

#include <stdexcept>

int collatz_conjecture::steps(int n) {
  if (n <= 0) {
    throw std::domain_error("only positive numbers allowed");
  }

  int count {0};

  while (n != 1) {
    ++count;

    n = n % 2 ? n * 3 + 1 : n / 2;
  }

  return count;
}
