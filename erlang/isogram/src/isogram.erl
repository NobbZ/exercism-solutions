-module(isogram).

-export([is_isogram/1]).


is_isogram(Phrase) -> is_isogram(Phrase, 2#11111111111111111111111111).

is_isogram([], _) -> true;
is_isogram([H|_], Set) when $a =< H, H =< $z, (Set band (1 bsl (H - $a))) =:= 0 -> false;
is_isogram([H|T], Set) when $A =< H, H =< $Z -> is_isogram([H - $A + $a|T], Set);
is_isogram([H|T], Set) when $a =< H, H =< $z ->
    Pattern = bnot (1 bsl (H - $a)),
    NewSet  = Set band Pattern,
    is_isogram(T, NewSet);
is_isogram([_|T], Set) -> is_isogram(T, Set).
