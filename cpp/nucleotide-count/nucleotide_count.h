#ifndef NUCLEOTIDE_COUNT_NUCLEOTIDE_COUNT_H
#define NUCLEOTIDE_COUNT_NUCLEOTIDE_COUNT_H

#include <string>
#include <map>

namespace dna {
  typedef std::map<char, int> nucleotide_count;

  class counter {
  public:
    counter (std::string strand);
    nucleotide_count nucleotide_counts() const;
    int count(char) const;

  private:
    const std::string strand;
    const nucleotide_count the_count;

    nucleotide_count count(std::string) const;
  };
}

#endif //NUCLEOTIDE_COUNT_NUCLEOTIDE_COUNT_H
