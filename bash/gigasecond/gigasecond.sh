#!/usr/bin/env bash

set -eu

main () {
    local date=${1}

    date --date="${date}Z +1000000000 second" \
         --utc \
         --iso-8601=seconds \
        | sed 's|\+.*||'
}

main "${@}"
