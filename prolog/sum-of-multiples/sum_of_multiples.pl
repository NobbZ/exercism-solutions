sum_of_multiples(Factors, Limit, Sum) :-
    Limit > 1,
    (
        Limit1 is Limit - 1,
        numlist(1, Limit1, Numbers),
        filter_multiples(Numbers, Factors, Multiples),
        sum_list(Multiples, Sum),
        !
    );
    Sum is 0.

filter_multiples([], _, []) :- !.
filter_multiples([N|T], Factors, Multiples) :-
    filter_multiples(T, Factors, MultiplesTail),
    (
        check(N, Factors),
        Multiples = [N|MultiplesTail], !;
        Multiples = MultiplesTail
    ).

check(Num, [H|T]) :-
    0 is mod(Num, H), !;
    check(Num, T).

%% Local Variables:
%% mode: prolog
%% End:
