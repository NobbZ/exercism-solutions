defmodule RPNCalculator.Exception do
  defmodule DivisionByZeroError do
    defexception message: "division by zero occurred"
  end

  defmodule StackUnderflowError do
    @message "stack underflow occurred"
    defexception message: @message

    def message(%__MODULE__{message: @message}), do: @message
    def message(%__MODULE__{message: msg}), do: "#{@message}, context: #{msg}"
  end

  def divide([]), do: raise(StackUnderflowError, "when dividing")
  def divide([_]), do: raise(StackUnderflowError, "when dividing")
  def divide([0, _]), do: raise(DivisionByZeroError)
  def divide([denominator, numerator]), do: div(numerator, denominator)
end
