#include <ctype.h>
#include <stdint.h>

#include "isogram.h"

#define empty (0)
#define set(s, c) (s | (1 << (tolower(c) - 'a')))
#define check(s, c) ((s & (1 << (tolower(c) - 'a'))) != 0)

bool is_isogram(const char phrase[])
{
    int32_t s = empty;

    if (!phrase)
      return false;

    for (int i = 0; phrase[i]; i++)
    {
        if (!isalpha(phrase[i]))
            continue;

        if (check(s, phrase[i]))
        {
            return false;
        }
        s = set(s, phrase[i]);
    }

    return true;
}
