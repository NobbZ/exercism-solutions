rna_transcription(DNA, RNA) :-
    string_chars(DNA, DNucs),
    do_rna_transcription(DNucs, RNucs),
    string_chars(RNA, RNucs).

do_rna_transcription([], []) :- !.
do_rna_transcription(['C'|DNucs], ['G'|RNucs]) :- do_rna_transcription(DNucs, RNucs), !.
do_rna_transcription(['G'|DNucs], ['C'|RNucs]) :- do_rna_transcription(DNucs, RNucs), !.
do_rna_transcription(['T'|DNucs], ['A'|RNucs]) :- do_rna_transcription(DNucs, RNucs), !.
do_rna_transcription(['A'|DNucs], ['U'|RNucs]) :- do_rna_transcription(DNucs, RNucs), !.

%% Local Variables:
%% mode: prolog
%% End:
