using System;
using System.Text;

public class Robot
{
    private string name;

    public string Name { get { return this.name; } }

    public Robot() {
        this.name = Robot.MakeRandomName();
    }

    public void Reset() {
        this.name = Robot.MakeRandomName();
    }

    private static string MakeRandomName() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        sb.Append((char) random.Next('A', 'Z' + 1));
        sb.Append((char) random.Next('A', 'Z' + 1));
        sb.Append((char) random.Next('0', '9' + 1));
        sb.Append((char) random.Next('0', '9' + 1));
        sb.Append((char) random.Next('0', '9' + 1));

        return sb.ToString();
    }
}