#!/usr/bin/env bash

set -o errexit
set -o nounset

## Setting up some common ASCII values
a=97  # lowercase letter a
z=122 # lowercase letter z
A=65  # uppercase letter A
Z=90  # uppercase letter Z
d0=48 # digit 0
d9=57 # digit 9

## Prints the argument, appending a newline.  If no argument is given,
## it just prints an empty line
println() {
  printf "%s\n" "${1:-}"
}

## Prints the character associated with the given ASCII-code
## (decimal).
chr() {
  (( "${1}" < 256 )) || return 1
  local octal=$(printf '%03o' "${1}")
  printf "\\${octal}"
}

## Prints the ASCII-code associated with the character given as
## argument.
ord() {
  if [ -n "$1" ]; then printf "%d" "'$1"; fi
}

## normalizes its stdin by lowercasing uppercase characters, keeping
## digits and removing everything else.  Normalized string is written
## to stdout.
normalize() {
  while read -n1 c; do
    if [ -z "$c" ]; then continue; fi

    local ord=$(ord "$c")
    if (( $a <= $ord & $ord <= $z )); then
      printf "%c" $c
    elif (( $A <= $ord & $ord <= $Z )); then
      # 32 is the difference between lowercase and uppercase section
      chr $(( $ord + 32 ))
    elif (( $d0 <= $ord & $ord <= $d9 )); then
      printf "%c" $c
    fi
  done
}

## Maps a to z and z to a and everything inbetween accordingly.
## Digits are left untouched, everything else is removed.  Consumes
## stdin and writes to stdout.
xcode() {
  while read -n1 c; do
    if [ -z "$c" ]; then continue; fi

    local ord=$(ord "$c")
    if (( $a <= $ord & $ord <= $z )); then
      # 219 is the magic offset ($z + $a)
      chr $(( 219 - $ord ))
    else
      printf "%c" $c
    fi
  done
}

## Inserts a blank every n characters, where n is the first argument.
## Consumes stdin and writes to stdout
group() {
  local pos=0
  while read -n1 c; do
    if (( $pos != 0 & $pos % $1 == 0 )); then printf " "; fi

    pos=$(( $pos + 1 ))

    printf "%c" $c
  done
}

encode() {
  println "${1}" | normalize | xcode | group 5
  println
}

decode() {
  println "${1}" | normalize | xcode
  println
}

"$1" "$2"

# Local Variables:
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
