(eval-when (:compile-toplevel :load-toplevel :execute)
  (quicklisp-client:quickload :arrows))

(defpackage :all-your-base
  (:use :cl)
  (:export :rebase))

(in-package :all-your-base)

(defun rebase (list-digits in-base out-base)
  (-<> list-digits
       (reduce (lambda (acc digit) (+ digit (* acc in-base))) <>)
       ((lambda (x) (loop while   (> x 0)
                          do      (setf x (floor x out-base))
                          collect (mod x out-base))) <>)))




;; (loop while (>= row 0) 
;;   do (setf row (- row 1))           ; or better: do (decf row)
;;   collect (findIndex row col))