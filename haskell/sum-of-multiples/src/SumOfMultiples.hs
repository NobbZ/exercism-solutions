module SumOfMultiples (sumOfMultiples) where

sumOfMultiples :: Integral a => [a] -> a -> a
sumOfMultiples factors limit = sum . filter p $ [1..(limit - 1)]
  where
    p x = x `divByOneOf` factors
    x `divByOneOf` ys = any (x `divBy`) ys
    x `divBy` y = x `mod` y == 0
