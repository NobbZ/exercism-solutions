{
  stdenv,
  lib,
  self,
}: let
  runExercise = slug: let
    pname = builtins.replaceStrings ["-"] ["_"] slug;
  in
    stdenv.mkDerivation {
      pname = "exercism_c_${pname}";
      version = "g${self.shortRev or "dirty"}";

      src = lib.cleanSource (./. + "/${slug}");

      phases = ["unpackPhase" "checkPhase"];

      doCheck = true;
      checkPhase = ''
        make | tee $out
      '';
    };
in {
  gigasecond = runExercise "gigasecond";
  hamming = runExercise "hamming";
  hello-world = runExercise "hello-world";
  isogram = runExercise "isogram";
}
