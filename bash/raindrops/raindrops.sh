#!/usr/bin/env bash

set -o errexit
set -o nounset

function make_sound () {
    local sound=$1
    local num=$2
    local when=$3

    local result=""

    if (( num % when == 0 )); then
        result="${sound}"
    fi

    printf "%s" "${result}"
}

function main () {
    local num=$1

    local result=""

    result+=$(make_sound "Pling" ${num} 3)
    result+=$(make_sound "Plang" ${num} 5)
    result+=$(make_sound "Plong" ${num} 7)

    if [[ -z "${result}" ]]; then
        printf "%d" ${1}
    else
        printf "%s" ${result}
    fi
}

main "$@"
