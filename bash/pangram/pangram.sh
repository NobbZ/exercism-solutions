#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
    local -A letter_set=()

    local phrase=$(echo ${1:-} | tr '[:upper:]' '[:lower:]')

    for (( i=0; $i < ${#phrase}; i++ )); do
        local letter=${phrase:$i:1}
        if [[ -z ${letter_set[$letter]:-} && $letter =~ ^[a-z]$ ]]; then
            letter_set[${letter}]=${letter}
        fi
    done

    if (( 26 == ${#letter_set[@]} )); then
        printf "true"
    else
        printf "false"
    fi
}

main "$@"
