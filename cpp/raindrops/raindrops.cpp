#include "raindrops.h"

#include <sstream>
#include <vector>

static const std::vector<std::pair<int, std::string>> drops {
  {3, "Pling"},
  {5, "Plang"},
  {7, "Plong"}
};

std::string raindrops::convert(int n) {
  std::ostringstream result;

  for (auto drop : drops) {
    if (n % drop.first == 0) result << drop.second;
  }

  std::string result_str = result.str();

  return result_str.empty() ? std::to_string(n) : result_str;
}
