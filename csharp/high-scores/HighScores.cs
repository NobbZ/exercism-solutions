using System;
using System.Collections.Generic;
using System.Linq;

public class HighScores
{
    private readonly List<int> list;

    public HighScores(List<int> list) {
        this.list = list;
    }

    public List<int> Scores() {
        return this.list;
    }

    public int Latest() {
        return this.list.Last();
    }

    public int PersonalBest() {
        return this.list.Max();
    }

    public List<int> PersonalTopThree() {
        return this.list.OrderByDescending(score => score).Take(3).ToList();
    }
}