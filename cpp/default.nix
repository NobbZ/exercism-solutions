{
  callPackage,
  stdenv,
  self,
  lib,
  cmake,
}: let
  runExercise = slug: let
    pname = builtins.replaceStrings ["-"] ["_"] slug;
  in
    stdenv.mkDerivation {
      pname = "exercism_cpp_${pname}";
      version = "g${self.shortRev or "dirty"}";

      src = lib.cleanSource (./. + "/${slug}");

      nativeBuildInputs = [cmake];

      phases = ["unpackPhase" "patchPhase" "configurePhase" "checkPhase"];

      postPatch = ''
        substituteInPlace CMakeLists.txt \
          --replace 'get_filename_component(exercise ''${CMAKE_CURRENT_SOURCE_DIR} NAME)' 'set(exercise "${slug}")'
      '';

      cmakeFlags = ["-DEXERCISM_RUN_ALL_TESTS=true"];

      doCheck = true;
      checkPhase = ''
        make | tee $out
      '';
    };
in {
  anagram = runExercise "anagram";
  atbash-cipher = runExercise "atbash-cipher";
  beer-song = runExercise "beer-song";
  binary = runExercise "binary";
  collatz-conjecture = runExercise "collatz-conjecture";
  hexadecimal = runExercise "hexadecimal";
  matching-brackets = runExercise "matching-brackets";
  reverse-string = runExercise "reverse-string";
  scrabble-score = runExercise "scrabble-score";
  space-exercise = runExercise "space-age";
  two-fer = runExercise "two-fer";
}
