#include "triangle.h"

#include <stdexcept>

bool all_positiv(double a, double b, double c) {
  return a > 0 && b > 0 && c > 0;
}

bool length_sum_valid(double a, double b, double c) {
  return a < (b + c) && b < (a + c) && c < (a + b);
}

bool valid_side_lengths(double a, double b, double c) {
  return all_positiv(a, b, c) && length_sum_valid(a, b, c);
}

triangle::triangletype triangle::kind(double a, double b, double c) {
  if (!a || !b || !c || !valid_side_lengths(a, b, c)) {
    throw std::domain_error {"Triangle with at least one zero-sized side!"};
  } else if (a == b && b == c) { // because of transitivity of equality a == c as well
    return equilateral;
  } else if (a == b || a == c || b == c) {
    return isosceles;
  } else {
    return scalene;
  }
}
