real((R, _), R).

imaginary((_, I), I).

conjugate((R, I), (R, I2)) :-
    I2 is -I.

abs((R, I), Abs) :-
    Abs is sqrt(R * R + I * I).

add((R1, I1), (R2, I2), (R3, I3)) :-
    R3 is R1 + R2,
    I3 is I1 + I2.

sub(Z1, Z2, Z3) :-
    add(Z2, Z3, Z1).

mul((R1, I1), (R2, I2), (R3, I3)) :-
    R3 is R1 * R2 - I1 * I2,
    I3 is I1 * R2 + R1 * I2.

div((R1, I1), (R2, I2), (R3, I3)) :-
    Div = R2 * R2 + I2 * I2,
    R3 is (R1 * R2 + I1 * I2) / Div,
    I3 is (I1 * R2 - R1 * I2) / Div.

%% Local Variables:
%% mode: prolog
%% End:
