#include "grade_school.h"

#include <algorithm>

grade_school::school::school() { }

grade_school::school_roster grade_school::school::roster() {
  return this->roster_;
}

grade_school::school& grade_school::school::add(std::string string, int i) {
  this->roster_[i].push_back(string);
  std::sort(this->roster_[i].begin(), this->roster_[i].end());
  return *this;
}

grade_school::school_grade grade_school::school::grade(int i) {
  if (1 == this->roster_.count(i)) {
    return this->roster_[i];
  }
  return school_grade();
}
