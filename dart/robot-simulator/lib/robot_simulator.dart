import 'package:robot_simulator/orientation.dart';
import 'package:robot_simulator/position.dart';

class Robot {
  Position position;
  Orientation orientation;

  Robot(this.position, this.orientation);

  void move(String commands) {
    commands.runes.forEach((command) {
      switch (command) {
        case 82: // "R"
          this.orientation = _turnCW(this.orientation);
          break;

        case 76: // "L"
          this.orientation = _turnCCW(this.orientation);
          break;

        case 65: // "A"
          this.position = _advance(this.position, this.orientation);
          break;

        default:
          throw new Exception("${command}");
      }
    });
  }
}

Orientation _turnCW(Orientation o) {
  switch (o) {
    case Orientation.north:
      return Orientation.east;

    case Orientation.east:
      return Orientation.south;

    case Orientation.south:
      return Orientation.west;

    case Orientation.west:
      return Orientation.north;
  }
}

Orientation _turnCCW(Orientation o) {
  switch (o) {
    case Orientation.north:
      return Orientation.west;

    case Orientation.west:
      return Orientation.south;

    case Orientation.south:
      return Orientation.east;

    case Orientation.east:
      return Orientation.north;
  }
}

Position _advance(Position pos, Orientation o) {
  switch (o) {
    case Orientation.north:
      pos.y += 1;
      break;

    case Orientation.east:
      pos.x += 1;
      break;

    case Orientation.south:
      pos.y -= 1;
      break;

    case Orientation.west:
      pos.x -= 1;
      break;
  }

  return pos;
}
