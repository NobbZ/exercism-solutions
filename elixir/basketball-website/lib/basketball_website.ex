defmodule BasketballWebsite do
  def extract_from_path(data, path) do
    segments = String.split(path, ".")

    Enum.reduce(segments, data, & &2[&1])
  end

  def get_in_path(data, path) do
    Kernel.get_in(data, String.split(path, "."))
  end
end
