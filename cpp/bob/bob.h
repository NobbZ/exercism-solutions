#ifndef WATCH_BOB_H
#define WATCH_BOB_H

#include <string>

namespace bob {
  std::string hey(std::string str);
};

#endif
