-module(rna_transcription).

-export([to_rna/1, test_version/0]).


to_rna(Strand) ->
  lists:foldr(fun
    ($C, Acc) when is_list(Acc) -> [$G | Acc];
    ($G, Acc) when is_list(Acc) -> [$C | Acc];
    ($T, Acc) when is_list(Acc) -> [$A | Acc];
    ($A, Acc) when is_list(Acc) -> [$U | Acc];
    (_,  _) -> error
  end, [], Strand).

test_version() -> 2.
