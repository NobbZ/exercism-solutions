defmodule Markdown do
  @spec parse(String.t()) :: String.t()
  def parse(markdown) do
    markdown
    |> String.split("\n")
    |> Enum.map_join(&process/1)
    |> fix_list()
  end

  defp process("#" <> _ = line), do: line |> parse_header_md_level() |> enclose_with_header_tag()
  defp process("*" <> _ = line), do: parse_list_md_level(line)
  defp process(line), do: line |> String.split() |> enclose_with_paragraph_tag()

  1..6
  |> Enum.map(fn n ->
    pre = ["#"] |> Stream.cycle() |> Enum.take(n) |> Enum.join()
    {n, pre <> " "}
  end)
  |> Enum.each(fn {n, pre} ->
    def parse_header_md_level(unquote(pre) <> text), do: {unquote(n), text}
  end)

  defp parse_list_md_level("* " <> text) do
    text = text |> String.split() |> join_words_with_tags()
    "<li>#{text}</li>"
  end

  defp enclose_with_header_tag({level, text}) do
    "<h#{level}>#{text}</h#{level}>"
  end

  defp enclose_with_paragraph_tag(text) do
    "<p>#{join_words_with_tags(text)}</p>"
  end

  defp join_words_with_tags(words) do
    words
    |> Enum.map(fn word -> replace_md_with_tag(word) end)
    |> Enum.join(" ")
  end

  defp replace_md_with_tag(word) do
    word |> replace_prefix_md() |> replace_suffix_md()
  end

  defp replace_prefix_md(word) do
    cond do
      word =~ ~r/^#{"__"}{1}/ -> String.replace(word, ~r/^#{"__"}{1}/, "<strong>", global: false)
      word =~ ~r/^[#{"_"}{1}][^#{"_"}+]/ -> String.replace(word, ~r/_/, "<em>", global: false)
      true -> word
    end
  end

  defp replace_suffix_md(word) do
    cond do
      word =~ ~r/#{"__"}{1}$/ -> String.replace(word, ~r/#{"__"}{1}$/, "</strong>")
      word =~ ~r/[^#{"_"}{1}]/ -> String.replace(word, ~r/_/, "</em>")
      true -> word
    end
  end

  defp fix_list(html) do
    html
    |> String.replace("<li>", "<ul><li>", global: false)
    |> String.replace_suffix("</li>", "</li></ul>")
  end
end
