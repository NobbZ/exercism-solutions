const nuc_map: { [key: string]: string } = {
    C: 'G',
    G: 'C',
    A: 'U',
    T: 'A'
}

export function toRna(dna: string): string {
    let rna = ''

    for (let i = 0; i < dna.length; i++) {
        const n = dna[i]
        if (!(n in nuc_map)) { throw new Error('Invalid input DNA.') }
        rna = rna + nuc_map[n]
    }

    return rna
}