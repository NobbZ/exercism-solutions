#include "series.h"

#include <stdexcept>

std::vector<int> series::digits(std::string const &input) {
  std::vector<int> result;

  for (int i = 0; i < input.length(); ++i) {
    result.push_back(input[i] - '0');
  }

  return result;
}

std::vector<std::vector<int>> series::slice(std::string const &input, size_t width) {
  if (width > input.length()) { throw std::domain_error{"Width is greater than string length!"}; }

  std::vector<std::vector<int>> result;

  for (size_t i = 0; i <= input.length() - width; ++i) {
    result.push_back(digits(input.substr(i, width)));
  }

  return result;
}
