#lang racket

(provide (contract-out
          [sum-of-squares (-> number? number?)]
          [square-of-sums (-> number? number?)]
          [difference     (-> number? number?)]))

(define (sum-of-squares n)
  (/ (* n (add1 n) (add1 (* 2 n))) 6))

(define (square-of-sums n)
  (sqr (/ (+ (sqr n) n) 2)))

(define (difference n)
  (- (square-of-sums n) (sum-of-squares n)))

(define (sqr n)
  (* n n))