import java.util.Optional;

class Scrabble {
    //                             A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P,  Q, R, S, T, U, V, W, X, Y,  Z
    private static int scores[] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};

    private Optional<Integer> score = Optional.empty();
    private char word[];

    Scrabble(String word) {
        this.word = word.toCharArray();
    }

    int getScore() {
        if (!this.score.isPresent()) {
            this.updateScore();
        }


        return this.score.get();
    }

    private void updateScore() {
        int score = 0;

        for (char c : this.word) {
            int value = 0;

            if ('A' <= c && c <= 'Z') {
                value = Scrabble.scores[c - 'A'];
            } else if ('a' <= c && c <= 'z') {
                value = Scrabble.scores[c - 'a'];
            }

            score += value;
        }

        this.score = Optional.of(score);
    }

}
