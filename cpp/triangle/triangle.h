#ifndef TRIANGLE_TRIANGLE_H
#define TRIANGLE_TRIANGLE_H

namespace triangle {
  enum triangletype {
    equilateral,
    isosceles,
    scalene
  };

  triangletype kind(double, double, double);
}

#endif //TRIANGLE_TRIANGLE_H
