#ifndef ROBOT_NAME_ROBOT_NAME_H
#define ROBOT_NAME_ROBOT_NAME_H

#include <string>

namespace robot_name {
  class robot {
  public:
    robot();

    robot& reset();

    std::string name() const;

  private:
    std::string _name;
  };
}

#endif //ROBOT_NAME_ROBOT_NAME_H
