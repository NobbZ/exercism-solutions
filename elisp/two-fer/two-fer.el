;;; two-fer.el --- Two-fer Exercise (exercism)

;;; Commentary:

;;; Code:

(defun two-fer (&optional name)
  "Return the string \"One for you, one for me.\".

If `NAME' is non-nil, the substring \"you\" is replaced with the
value of `NAME'."
  (format "One for %s, one for me."
          (or name "you")))

(provide 'two-fer)
;;; two-fer.el ends here
