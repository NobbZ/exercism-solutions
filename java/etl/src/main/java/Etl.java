import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class Etl {
  public static Map<String, Integer> transform(Map<Integer, List<String>> old) {
    return old.entrySet().stream().flatMap(entry -> {
      int score = entry.getKey();
      return entry.getValue().stream().map(letter -> new AbstractMap.SimpleEntry<>(letter.toLowerCase(), score));
    }).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
  }
}
