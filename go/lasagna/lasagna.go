package lasagna

func OvenTime() int {
	return 40
}

func RemainingOvenTime(timeInOven int) int {
	return OvenTime() - timeInOven
}

func PreparationTime(layers int) int {
	return layers * 2
}

func ElapsedTime(layers int, timeInOven int) int {
	return PreparationTime(layers) + timeInOven
}
