#ifndef SPACE_AGE_SPACE_AGE_H
#define SPACE_AGE_SPACE_AGE_H

#include <cstdint>

namespace space_age {
  typedef std::uint_least64_t second;
  typedef double year;

  class space_age {
  public:
    space_age(second);

    second seconds() const;

    year on_earth() const;
    year on_mercury() const;
    year on_neptune() const;
    year on_uranus() const;
    year on_saturn() const;
    year on_jupiter() const;
    year on_mars() const;
    year on_venus() const;

  private:
    second base_value;
  };
}

#endif //SPACE_AGE_SPACE_AGE_H