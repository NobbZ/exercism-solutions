#if !defined(MATCHING_BRACKETS_H)
#define MATCHING_BRACKETS_H

#include <string>

namespace matching_brackets {
  bool check(std::string const &);
}  // namespace matching_brackets

#endif // MATCHING_BRACKETS_H
