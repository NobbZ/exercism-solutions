#include "binary.h"

unsigned int binary::convert(std::string input) {
  unsigned int result {0};

  for (auto current : input) {
    result <<= 1;
    switch (current) {
    case '1': result |= 1;
    case '0': break;
    default: return 0;
    }
  }

  return result;
}