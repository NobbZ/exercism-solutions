-module(forth).

-export([evaluate/1]).

-record(state, {instrs, defs, stack}).


evaluate(Exprs) ->
  State = #state{instrs = instructions(), defs = #{}, stack = []},
  Exprs0 = lists:map(fun string:to_upper/1, Exprs),
  evaluate(Exprs0, State).

evaluate([], #state{stack=Stack}) ->
  lists:reverse(Stack);
evaluate([":" ++ Def|Instrs], State) ->
  evaluate(Instrs, def(Def, State));
evaluate([Instr|Instrs], State) ->
  evaluate(Instrs, exec(Instr, State)).

def(Def, State = #state{defs = Defs}) ->
  [Name|Def1] = string:tokens(Def, " "),

  false = lists:all(fun is_digit/1, Name),

  Def2 = lists:sublist(Def1, length(Def1) - 1),

  Def3 = lists:map(fun (Tok) ->
    case lists:all(fun is_digit/1, Tok) of
      true ->
        Tok;

      false ->
        handle_token(Tok, State,
          fun (_) -> Tok end,
          fun (Replace) -> Replace end
        )
    end
  end, Def2),

  Def4 = lists:flatten(lists:join(" ", Def3)),
  State#state{defs = Defs#{Name => Def4}}.

exec(Instr, State) ->
  Toks = string:tokens(Instr, " "),
  lists:foldl(fun (Tok, Acc = #state{stack = Stack}) ->
    case lists:all(fun is_digit/1, Tok) of
      true -> % this is a number, put on stack
        Acc#state{stack = [list_to_integer(Tok)|Stack]};

      false -> % a word
        handle_token(Tok, Acc,
          fun (Fun) -> Acc#state{stack = Fun(Stack)} end,
          fun (Def) -> exec(Def, Acc) end
        )
    end
  end, State, Toks).

handle_token(Token, State, WhenInstr, WhenDef) ->
  case {maps:get(Token, State#state.defs, undefined), maps:get(Token, State#state.instrs, undefined)} of
    {undefined, undefined} ->
      error(undef);

    {undefined, Instr} ->
      WhenInstr(Instr);

    {Def, _} ->
      WhenDef(Def)
  end.

instructions() ->
  #{
    "+" => fun ([B, A|Stack]) -> [A + B|Stack] end,
    "-" => fun ([B, A|Stack]) -> [A - B|Stack] end,
    "*" => fun ([B, A|Stack]) -> [A * B|Stack] end,
    "/" => fun ([B, A|Stack]) -> [A div B|Stack] end,
    "DUP" => fun ([A|Stack]) -> [A, A|Stack] end,
    "DROP" => fun ([_|Stack]) -> Stack end,
    "SWAP" => fun ([B, A|Stack]) -> [A, B|Stack] end,
    "OVER" => fun ([B, A|Stack]) -> [A, B, A|Stack] end
  }.

is_digit(C) when $0 =< C, C =< $9 -> true;
is_digit(_) -> false.