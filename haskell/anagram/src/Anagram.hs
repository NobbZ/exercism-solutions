module Anagram (anagramsFor) where

import Data.Char
import Data.List

anagramsFor :: String -> [String] -> [String]
anagramsFor w = filter isAnagram
  where
    lower = map toLower w
    sorted = sort lower
    isAnagram w' =
      let other = map toLower w' in
      lower /= other && sorted == sort other