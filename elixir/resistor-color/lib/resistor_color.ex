defmodule ResistorColor do
  @moduledoc false

  @colors [
    "black",
    "brown",
    "red",
    "orange",
    "yellow",
    "green",
    "blue",
    "violet",
    "grey",
    "white"
  ]

  @spec colors() :: list(String.t())
  def colors(), do: @colors

  @spec code(String.t()) :: integer()
  @colors
  |> Enum.with_index(0)
  |> Enum.each(fn {color, idx} ->
    def code(unquote(color)), do: unquote(idx)
  end)
end
