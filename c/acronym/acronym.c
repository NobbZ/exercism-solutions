#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "acronym.h"

#define RESULT_BUFFER_LENGTH 20

enum Class { BOUNDARY, UPPER, LOWER };

enum Class getCharType(char);

char *abbreviate(const char *phrase) {
  if (phrase == NULL || phrase[0] == '\0')
    return NULL;

  char *result = malloc(RESULT_BUFFER_LENGTH);
  unsigned char pos = 0;
  enum Class last = BOUNDARY;

  result = memset(result, '\0', RESULT_BUFFER_LENGTH);

  for (; *phrase; ++phrase) {
    if (last == BOUNDARY && isalpha(*phrase)) {
      result[pos++] = toupper(*phrase);
    } else if (isupper(*phrase) && last != UPPER) {
      result[pos++] = *phrase;
    }
    last = getCharType(*phrase);
  }

  return realloc(result, !pos ? 0 : pos + 1);
}

enum Class getCharType(char c) {
  if (isalpha(c)) {
    return islower(c) ? LOWER : UPPER;
  } else if (c == '\'') {
      return LOWER;
  }
  return BOUNDARY;
}