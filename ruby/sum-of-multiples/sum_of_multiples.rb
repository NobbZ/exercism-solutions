class SumOfMultiples
  def initialize(*multiples)
    @multiples = multiples
  end

  def to(n)
    (1...n).select do |x|
      @multiples.any? { |m| x % m == 0 }
    end.reduce(0, &:+)
  end
end

module BookKeeping
  VERSION = 2
end