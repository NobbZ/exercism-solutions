% -*- mode: prolog -*-

create((X,Y)) :-
    0 =< X, X =< 7,
    0 =< Y, Y =< 7.

attack(W, B) :-
    row(W, B), !;
    col(W, B), !;
    diag(W, B), !.

row((X, _), (X, _)).

col((_, Y), (_, Y)).

diag((WX, WY), (BX, BY)) :-
    D1 is abs(WX - BX),
    D2 is abs(WY - BY),
    D1 =:= D2.
