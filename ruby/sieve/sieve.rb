class Sieve
  attr_reader :primes

  def initialize(n)
    @n = n
    @primes = sieve_to_n
  end

  private

  def sieve_to_n
    @primes = (2..@n).to_a
    @primes.each do |prime|
      @primes.reject! do |candidate|
        candidate != prime && candidate % prime == 0
      end
    end
  end
end

module BookKeeping
  VERSION = 1
end
