#include "bob.h"

#include <algorithm>
#include <iostream>

static const std::string ANSWER_SILENCE  = "Fine. Be that way!";
static const std::string ANSWER_QUESTION = "Sure.";
static const std::string ANSWER_OTHER    = "Whatever.";
static const std::string ANSWER_SHOUT    = "Whoa, chill out!";

bool isSilence(std::string const &str) {
  return std::all_of(str.begin(), str.end(), isspace);
}

bool isShout(std::string const &str) {
  return std::none_of(str.begin(), str.end(), islower)
      && std::any_of(str.begin(), str.end(), isalpha);
}

bool isQuestion(std::string const &str) {
  auto it   = str.crbegin();
  auto stop = str.crend();
  while (isspace(*it) && it < stop) { ++it; }
  return *it == '?';
}

std::string bob::hey(std::string str) {
  if (isSilence(str))  { return ANSWER_SILENCE; }
  if (isShout(str))    { return ANSWER_SHOUT; }
  if (isQuestion(str)) { return ANSWER_QUESTION; }
  return ANSWER_OTHER;
}
