import 'dart:math';

List<int> _getDigits(int n) {
  List<int> result = [];

  while (n > 0) {
    result.add(n % 10);
    n ~/= 10;
  }

  return result.reversed.toList();
}

class ArmstrongNumbers {
  // Put your code here
  bool isArmstrongNumber(int n) {
    var digits = _getDigits(n);
    var digitCount = digits.length;

    return n ==
        digits
            .map((e) => pow(e, digitCount))
            .fold(0, (acc, e) => (acc as int) + e);
  }
}
