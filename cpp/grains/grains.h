#ifndef GRAINS_GRAINS_H
#define GRAINS_GRAINS_H

#include <cstdint>

namespace grains {
  typedef std::uint_least64_t grain_count;
  typedef std::uint_least8_t square_number;

  grain_count square(square_number);
  grain_count total();
}

#endif //GRAINS_GRAINS_H

