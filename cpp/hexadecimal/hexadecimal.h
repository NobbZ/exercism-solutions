#ifndef HEXADECIMAL_HEXADECIMAL_H
#define HEXADECIMAL_HEXADECIMAL_H

#include <cstdint>
#include <string>

namespace hexadecimal {
   std::size_t convert(std::string const & hex);
}

#endif //HEXADECIMAL_HEXADECIMAL_H
