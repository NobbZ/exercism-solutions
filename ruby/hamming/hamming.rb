class Hamming
  def self.compute(strand1, strand2)
    raise ArgumentError if strand1.length != strand2.length
    strand1.chars.zip(strand2.chars).count { |x, y| x != y }
  end
end

class BookKeeping
  VERSION = 3
end