#ifndef BINARY_BINARY_H
#define BINARY_BINARY_H

#include <string>

namespace binary {
  unsigned int convert(std::string input);
}

#endif //BINARY_BINARY_H