let is_equilateral a b c =
  match (a, b, c) with 0, 0, 0 -> false | _, _, _ -> a = b && b = c

let is_isosceles a b c =
  (a = b || b = c || c = a) && a + b >= c && b + c >= a && c + a >= b

let is_scalene a b c =
  a <> b && b <> c && c <> a && a + b >= c && b + c >= a && c + a >= b
