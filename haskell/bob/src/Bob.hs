module Bob (responseFor) where

import qualified Data.Char as Char

responseFor :: String -> String
responseFor = go . trim
  where
    go s | isShout s && isQuestion s = "Calm down, I know what I'm doing!"
         | isSilence s               = "Fine. Be that way!"
         | isShout s                 = "Whoa, chill out!"
         | isQuestion s              = "Sure."
         | otherwise                 = "Whatever."

isSilence :: String -> Bool
isSilence = all Char.isSpace

isShout :: String -> Bool
isShout str = not (any Char.isLower str) && any Char.isLetter str

isQuestion :: String -> Bool
isQuestion str = str `endswith` '?'

endswith :: Eq a => [a] -> a -> Bool
endswith xs e = last xs == e

trim :: String -> String
trim = reverse . dropWhile Char.isSpace . reverse . dropWhile Char.isSpace
