class School
  def initialize
    @roster = Hash.new { [] }
  end

  def students(grade)
    @roster[grade]
  end

  def add(name, grade)
    @roster[grade] <<= name
    @roster[grade].sort!
  end

  def students_by_grade
    @roster.map do |grade, students|
      {grade: grade, students: students}
    end.sort_by { |elem| elem[:grade] }
  end
end

module BookKeeping
  VERSION = 3
end