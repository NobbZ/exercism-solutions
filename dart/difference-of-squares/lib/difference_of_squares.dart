class DifferenceOfSquares {
  int squareOfSum(int n) {
    var sum = (n * n + n) ~/ 2;

    return sum * sum;
  }

  int sumOfSquares(int n) {
    return (n * (n + 1) * (2 * n + 1)) ~/ 6;
  }

  int differenceOfSquares(int n) {
    return squareOfSum(n) - sumOfSquares(n);
  }
}
