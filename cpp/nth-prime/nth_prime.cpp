#include "nth_prime.h"

#include <vector>
#include <stdexcept>

static std::vector<std::uintmax_t> primes {2, 3};

bool is_prime(std::uintmax_t n) {
  for (auto prim_div : primes) {
    if (prim_div == n) { return true; }
    if (prim_div * prim_div > n) { return true; }
    if (!(n % prim_div)) { return false; }
  }

}

std::uintmax_t update_primes_until(std::uintmax_t new_size) {
  auto latest_prime = primes.back();

  for (std::uintmax_t i = 1; primes.size() < new_size; ++i) {
    std::uintmax_t candidate1 = 6 * i - 1; // Every (known) prime number greater than 3 does follow these rules,
    std::uintmax_t candidate2 = 6 * i + 1; // this is also known as Wheel-Sieve AFAIK.
    if (candidate1 > latest_prime && is_prime(candidate1)) { primes.push_back(candidate1); }
    if (candidate2 > latest_prime && is_prime(candidate2)) { primes.push_back(candidate2); }
  }

  return prime::nth(new_size);
}

std::uintmax_t prime::nth(std::intmax_t n) {
  if (n <= 0) { throw std::domain_error {"I don't like negative numbers!"}; }
  if (primes.size() >= n) { return primes[n - 1]; };
  return update_primes_until(n);
}
