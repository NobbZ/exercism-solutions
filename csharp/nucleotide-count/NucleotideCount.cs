using System;
using System.Collections.Generic;
using System.Linq;

public static class NucleotideCount {
    public static IDictionary<char, int> Count(string sequence) {
        Dictionary<char, int> count = new Dictionary<char, int> {
            ['A'] = 0,
            ['C'] = 0,
            ['G'] = 0,
            ['T'] = 0,
        };

        foreach (char nuc in sequence) {
            try {
                count[nuc] += 1;
            } catch (KeyNotFoundException) {
                throw new ArgumentException();
            }
        }

        return count;
    }
}