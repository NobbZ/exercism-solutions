#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
  printf "One for %s, one for me.\n" ${1:-you}
}

main "$@"

# Local Variables:
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
