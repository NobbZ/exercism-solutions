#ifndef SERIES_SERIES_H
#define SERIES_SERIES_H

#include <vector>
#include <string>

namespace series {
  std::vector<int> digits(std::string const &input);

  std::vector<std::vector<int>> slice(std::string const &input, size_t width);
}

#endif //SERIES_SERIES_H
