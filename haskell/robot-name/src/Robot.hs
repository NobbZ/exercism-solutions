module Robot (Robot, mkRobot, resetName, robotName) where

import Control.Concurrent.STM (TVar, atomically, newTVar, readTVar, writeTVar)
import System.Random (randomRIO)

newtype Robot = Robot (TVar String)

mkName :: IO String
mkName = mapM randomRIO pat
  where
    pat = [l, l, n, n, n]
    l = ('A', 'Z')
    n = ('0', '9')

mkRobot :: IO Robot
mkRobot = fmap Robot $ mkName >>= atomically . newTVar

resetName :: Robot -> IO ()
resetName (Robot name) = mkName >>= atomically . writeTVar name

robotName :: Robot -> IO String
robotName (Robot name) = atomically $ readTVar name
