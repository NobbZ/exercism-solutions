#include "scrabble_score.h"

#include <numeric>

static const std::uint8_t letter_values[] {
//  A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P,  Q, R, S, T, U, V, W, X, Y,  Z
    1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10
};

scrabble_score::word_value scrabble_score::score(scrabble_score::word const &word) {
  return (word_value) std::accumulate(word.cbegin(), word.cend(), 0, [](word_value const &acc, char const &current) {
    return acc + letter_values[tolower(current) - 'a'];
  });
}
