defmodule BirdCount do
  def today([]), do: nil
  def today([n | _]), do: n

  def increment_day_count([]), do: [1]
  def increment_day_count([h | t]), do: [h + 1 | t]

  def has_day_without_birds?([]), do: false
  def has_day_without_birds?([0 | _]), do: true
  def has_day_without_birds?([_ | t]), do: has_day_without_birds?(t)

  def total(list), do: total(list, 0)

  defp total([], sum), do: sum
  defp total([x | xs], sum), do: total(xs, sum + x)

  def busy_days(list), do: busy_days(list, 0)

  defp busy_days([], count), do: count
  defp busy_days([x | xs], count) when x >= 5, do: busy_days(xs, count + 1)
  defp busy_days([_ | xs], count), do: busy_days(xs, count)
end
