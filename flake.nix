{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nobbz.url = "github:nobbz/nixos-config";

  inputs.naersk = {
    url = "github:nmattia/naersk";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  inputs.oxalica = {url = "github:oxalica/rust-overlay";};

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  } @ inputs:
    flake-utils.lib.eachSystem ["x86_64-linux"] (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [inputs.oxalica.overlays.default];
      };

      naerskLib = pkgs.callPackage inputs.naersk {inherit (rustTooling) cargo rustc;};

      rustTooling = let
        rust = pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default);
      in rec {
        inherit rust;
        rustc = rust;
        cargo = rust;
        rustPlatform = pkgs.makeRustPlatform {
          inherit rustc cargo;
        };
      };

      callPackage = pkgs.lib.callPackageWith (pkgs
        // {
          inherit naerskLib rustTooling self;
          # inherit (rustTooling) rustc cargo rustPlatform;
        });

      exercism = pkgs.recurseIntoAttrs {
        bash = pkgs.recurseIntoAttrs (pkgs.callPackage ./bash {inherit self;});
        c = pkgs.recurseIntoAttrs (pkgs.callPackage ./c {inherit self;});
        # clojure = pkgs.recurseIntoAttrs (pkgs.callPackage ./clojure { });
        coffeescript = pkgs.recurseIntoAttrs (pkgs.callPackage ./coffeescript {inherit self;});
        cpp = pkgs.recurseIntoAttrs (pkgs.callPackage ./cpp {inherit self;});
        javascript = pkgs.recurseIntoAttrs (pkgs.callPackage ./javascript {inherit self;});
      };
    in {
      formatter = inputs.nobbz.formatter.${system};

      packages = flake-utils.lib.flattenTree (pkgs.recurseIntoAttrs exercism);

      checks = self.packages.${system};

      devShells.default = pkgs.mkShell {
        packages = builtins.attrValues {
          inherit (pkgs) nil;
        };
      };

      devShells.erlang = pkgs.mkShell {
        packages = [pkgs.erlang pkgs.rebar3];
        inputsFrom = builtins.attrValues {inherit (self.devShells.${system}) default;};
      };

      devShells.rust = pkgs.mkShell {
        packages = [pkgs.rust-analyzer (rustTooling.rust.override {extensions = ["rustfmt-preview" "rust-src" "rls-preview" "rust-analysis"];}) pkgs.rustfmt];
        inputsFrom = builtins.attrValues {inherit (self.devShells.${system}) default;};
      };

      devShells.zig = pkgs.mkShell {
        packages = [ pkgs.zig pkgs.zls ];
        inputsFrom = builtins.attrValues {inherit (self.devShells.${system}) default;};
        };

      devShells.common-lisp = pkgs.callPackage ./common-lisp/shell.nix {};
    });
}
