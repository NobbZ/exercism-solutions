#ifndef ROMAN_NUMERALS_ROMAN_NUMERALS_H
#define ROMAN_NUMERALS_ROMAN_NUMERALS_H

#include <string>
#include <cstdint>

namespace roman {
  std::string convert(std::uintmax_t n);
}

#endif //ROMAN_NUMERALS_ROMAN_NUMERALS_H
