-module(accumulate).

-export([accumulate/2, test_version/0]).

accumulate(Fn, Ls) when is_function(Fn, 1), is_list(Ls) ->
  lists:foldr(fun (X, Acc) -> [Fn(X)|Acc] end, [], Ls).

test_version() -> 1.
