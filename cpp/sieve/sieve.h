#ifndef SIEVE_SIEVE_H
#define SIEVE_SIEVE_H

#include <cstdint>
#include <vector>

namespace sieve {
  const std::vector<int> primes(int upper_bound);
}

#endif //SIEVE_SIEVE_H
