#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
    local number=$1
    local digits=${#number}

    local sum=0

    for (( i=0; i < ${digits}; i++ )); do
        sum=$(( $sum + ${number:$i:1} ** ${digits} ))
    done

    if (( sum != number )); then
        printf "false\n"
        return 1
    fi

    printf "true\n"
}

main "$@"