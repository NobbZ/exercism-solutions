#ifndef WORDCOUNT_H_GUARD
#define WORDCOUNT_H_GUARD

#include <map>

namespace word_count {
  /** How often a word occurs */
  typedef std::map<std::string, int> histogram;

  /** returns the histogram to a given text */
  histogram words(std::string text);
}

#endif
