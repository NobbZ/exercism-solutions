module LeapYear
  ( isLeapYear
  )
where

isLeapYear :: Integral a => a -> Bool
isLeapYear year = 4 |? year && not (100 |? year) || 400 |? year

(|?) :: Integral a => a -> a -> Bool
n |? m = m `mod` n == 0
