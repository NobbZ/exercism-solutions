-module(pangram).

-export([is_pangram/1]).


is_pangram(Sentence) ->
    is_pangram(Sentence, 0).

is_pangram(_, 2#11111111111111111111111111) -> true;
is_pangram([], _) -> false;
is_pangram([H|T], Set) when $A =< H, H =< $Z -> is_pangram([H - $A + $a|T], Set);
is_pangram([H|T], Set) when $a =< H, H =< $z ->
    NewSet = Set bor (1 bsl (H - $a)),
    is_pangram(T, NewSet);
is_pangram([_|T], Set) -> is_pangram(T, Set).
