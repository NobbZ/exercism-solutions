#include "etl.h"

etl::new_format etl::transform(etl::old_format const &old_data) {
  new_format result {};

  for (auto entry_set : old_data) {
    for (auto letter : entry_set.second) {
      result[std::tolower(letter)] = entry_set.first;
    }
  }

  return result;
}