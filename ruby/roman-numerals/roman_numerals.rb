module Roman
  PATTERNS = [[1000, "M"], [900, "CM"], [500, "D"], [400, "CD"], [100, "C"], [90, "XC"], [50, "L"], [40, "XL"], [10, "X"], [9, "IX"], [5, "V"], [4, "IV"], [1, "I"]]

  def to_roman
    num = self
    PATTERNS.reduce "" do |acc, pat|
      res = num.divmod(pat[0])
      num = res[1]
      acc + pat[1] * res[0]
    end
  end
end

class Fixnum
  include Roman
end

module BookKeeping
  VERSION = 2
end