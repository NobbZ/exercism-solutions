#include "difference_of_squares.h"

unsigned int squares::square_of_sums(int const n) {
  int sum = ((n * n) + n) / 2;
  return (unsigned int) (sum * sum);
}

unsigned int squares::sum_of_squares(int const n) {
  return (unsigned int) ((n * (n + 1) * (2 * n + 1)) / 6);
}

unsigned int squares::difference(int const n) {
  return square_of_sums(n) - sum_of_squares(n);
}
