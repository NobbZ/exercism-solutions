;;; hamming.el --- Hamming (exercism)

;;; Commentary:

;;; Code:

(require 'seq)

(defun hamming-distance (a b)
  "Calculate hamming distance of two strings A and B."
  (let ((compare (lambda (x y)
                   (if (= x y) 0 1))))
    (if (not (= (length a)
                (length b)))
        (signal 'non-equal-length)
      (apply #'+ (seq-mapn compare a b)))))

(provide 'hamming)
;;; hamming.el ends here
