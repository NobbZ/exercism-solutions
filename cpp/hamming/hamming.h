#ifndef HAMMING_HAMMING_H
#define HAMMING_HAMMING_H

#include <string>

namespace hamming {
  unsigned int compute(std::string const&, std::string const&);
}

#endif // HAMMING_HAMMING_H