module ListOps (length, reverse, map, filter, foldr, foldl', (++), concat) where

import           Prelude hiding (concat, filter, foldr, length, map, reverse,
                          (++))

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' _f acc []     = acc
foldl' f  acc (x:xs) = let acc' = f acc x in
  acc' `seq` foldl' f acc' xs

foldr :: (a -> b -> b) -> b -> [a] -> b
foldr _f acc []     = acc
foldr f  acc (x:xs) = x `f` foldr f acc xs

length :: [a] -> Int
length = foldl' (\len _ -> succ len) 0

reverse :: [a] -> [a]
reverse = foldl' (flip (:)) []

map :: (a -> b) -> [a] -> [b]
map f = foldr (\x xs -> f x:xs) []

filter :: (a -> Bool) -> [a] -> [a]
filter p = foldr (\x xs -> if p x then x:xs else xs) []

(++) :: [a] -> [a] -> [a]
(++) = flip $ foldr (:)

concat :: [[a]] -> [a]
concat = foldr (++) []
