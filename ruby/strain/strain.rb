module ArrayStrainHelper
  def keep(&block)
    self.reduce(Array.new) do |arr, e|
      arr <<= e if block.call e
      arr
    end
  end

  def discard(&block)
    self.keep { |e| !block.call e }
  end
end

class Array
  include ArrayStrainHelper
end
