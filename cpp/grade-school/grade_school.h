#ifndef GRADE_SCHOOL_GRADE_SCHOOL_H
#define GRADE_SCHOOL_GRADE_SCHOOL_H

#include <map>
#include <vector>
#include <string>


namespace grade_school {
  typedef std::vector<std::string>    school_grade;
  typedef std::map<int, school_grade> school_roster;

  class school {
  public:
    school();

    school_roster roster();

    school_grade  grade(int);

    school& add(std::string, int);

  private:
    school_roster roster_ {};
  };
}

#endif //GRADE_SCHOOL_GRADE_SCHOOL_H
