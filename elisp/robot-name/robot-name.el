;;; robot-name.el --- Robot Name (exercism)

;;; Commentary:
;;
;; Build a robot with a name like AA000, that can be reset
;; to a new name.  Instructions are in README.md
;;

;;; Code:

(defun build-robot ()
  `(robot :name ,(robot-name--gen-name)))

(defun robot-name (robot)
  (let ((data (cdr robot)))
    (plist-get data :name)))

(defun reset-robot (robot)
  (let* ((data (cdr robot))
         (new-data (plist-put data :name (robot-name--gen-name))))
    `(robot . ,new-data)))

(defun robot-name--random-char (input)
  (let ((i (random (length input))))
    (substring input i (1+ i))))

(defun robot-name--gen-name ()
  (let ((alphabet "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        (digits "0123456789"))
    (concat (robot-name--random-char alphabet)
            (robot-name--random-char alphabet)
            (robot-name--random-char digits)
            (robot-name--random-char digits)
            (robot-name--random-char digits))))

(provide 'robot-name)
;;; robot-name.el ends here
