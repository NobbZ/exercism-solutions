module Hamming

let distance (a: string) (b: string): int option =
    let counter acc a b =
        if a = b then acc else acc + 1

    if String.length a <> String.length b
    then None
    else Some(Seq.fold2 counter 0 a b)