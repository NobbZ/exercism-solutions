// @ts-check

/**
 * @param {string} color
 * 
 * @returns {number}
 */
export const colorCode = (color) => {
  return COLORS.indexOf(color);
};

/** @type {string[]} */
export const COLORS = ["black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"];
