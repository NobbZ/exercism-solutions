(defmodule leap
  (export (leap-year 1)))

(defun leap-year
  ([y] (when (=:= 0 (rem y 400))) 'true)
  ([y] (when (=:= 0 (rem y 100))) 'false)
  ([y] (when (=:= 0 (rem y   4))) 'true)
  ([_] 'false))
