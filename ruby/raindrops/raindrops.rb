class Raindrops
  MAPPINGS = {
    3 => 'Pling',
    5 => 'Plang',
    7 => 'Plong'
  }

  def self.convert(n)
    result = MAPPINGS.select { |factor, _| n % factor == 0 }
      .values
      .join

    result.empty? ? n.to_s : result
  end
end

class BookKeeping
  VERSION = 3
end