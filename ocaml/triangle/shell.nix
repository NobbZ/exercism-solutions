{
  pkgs ?
    import
    (builtins.fetchTarball
      "https://github.com/NixOS/nixpkgs-channels/archive/dc80d7bc4a244120b3d766746c41c0d9c5f81dfa.tar.gz")
    {
      config.allowUnfree = true;
    },
}: let
  inherit
    (pkgs.ocaml-ng.ocamlPackages_4_10)
    buildDunePackage
    uuseg
    re
    odoc
    ocaml-migrate-parsetree
    stdio
    fpath
    cmdliner
    menhir
    uutf
    fix
    dune_2
    ocaml
    core
    ounit
    utop
    findlib
    ;
  inherit
    (pkgs)
    gnum4
    opam
    vscode-with-extensions
    vscode-utils
    ocaml-lsp
    fetchFromGitHub
    ;

  ocaml-format = buildDunePackage rec {
    pname = "ocamlformat";
    version = "0.14.2";

    useDune2 = true;

    buildInputs = [
      uuseg
      re
      odoc
      ocaml-migrate-parsetree
      stdio
      fpath
      cmdliner
      menhir
      uutf
      fix
    ];

    src = fetchFromGitHub {
      owner = "ocaml-ppx";
      repo = "ocamlformat";
      rev = version;
      sha256 = "03n3d3l83mw4cfdz8w87hn66pzyvklgq1gwdwi8kndsmslwfcp4y";
    };
  };
in
  pkgs.mkShell {
    buildInputs = [
      core
      dune_2
      findlib
      gnum4
      ocaml
      ocaml-format
      opam
      ounit
      utop
    ];
  }
