#include "sieve.h"

#if __GNUC__ < 8
#include <experimental/optional>
typedef std::experimental::optional<bool> opt_bool;
#else
#include <optional>
typedef std::optional<bool> opt_bool;
#endif


std::vector<opt_bool> prepare(int upper_bound) {
  std::vector<opt_bool> result(upper_bound, opt_bool {});

  result[0] = opt_bool {false};

  return result;
}

const std::vector<int> sieve::primes(int upper_bound) {
  std::vector<int> result {};
  std::vector<opt_bool> the_list {prepare(upper_bound)};

  for (int i = 0; i < upper_bound; ++i) {
    if (!the_list[i]) { // We haven't seen this number (i+1) yet
      the_list[i] = opt_bool {true}; // So it is prime!

      for (int j = 2 * i + 1; j < upper_bound; j += i+1) {
        the_list[j] = opt_bool {false}; // j+1 is a multiple of i+1, so it is not a prime.
      }
    }

    if (*the_list[i]) { // if i+1 is a prime...
      result.push_back(i+1); // we add it to the result set
    }
  }

  result.shrink_to_fit();
  return result;
}