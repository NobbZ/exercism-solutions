defmodule Bob do
  defguardp is_silence(str) when str === ""

  @spec hey(String.t()) :: String.t()
  def hey(input) when is_binary(input) do
    stripped = String.trim(input)

    question? = question?(stripped)
    shout? = shout?(stripped)

    cond do
      shout? and question? -> "Calm down, I know what I'm doing!"
      shout? -> "Whoa, chill out!"
      question? -> "Sure."
      is_silence(stripped) -> "Fine. Be that way!"
      true -> "Whatever."
    end
  end

  @spec shout?(String.t()) :: boolean
  defp shout?(str) do
    String.upcase(str) == str and not (String.downcase(str) == str)
  end

  @spec question?(String.t()) :: boolean
  defp question?(str) do
    String.ends_with?(str, "?")
  end
end
