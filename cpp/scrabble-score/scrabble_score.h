#ifndef SCRABBLE_SCORE_SCRABBLE_SCORE_H
#define SCRABBLE_SCORE_SCRABBLE_SCORE_H

#include <cstdint>
#include <string>

namespace scrabble_score {
  typedef std::uint_fast16_t word_value;
  typedef std::string        word;

  word_value score(word const &word);
}

#endif //SCRABBLE_SCORE_SCRABBLE_SCORE_H
