#include "atbash_cipher.h"

std::string atbash_cipher::encode(std::string s ) {
  std::string result;
  int n = 0;

  for (char const &c: s) {
    if ('a' <= c && c <= 'z') {
      result.push_back('a' + ('z' - c));
      ++n;
    } else if ('A' <= c && c <= 'Z') {
      result.push_back('a' + ('Z' - c));
      ++n;
    } else if ('0' <= c && c <= '9') {
      result.push_back(c);
      ++n;
    }

    if (n > 0 && n % 5 == 0 && result.back() != ' ') {
      result.push_back(' ');
    }
  }

  if (result.back() == ' ') {
    result.pop_back();
  }

  return result;
}

std::string atbash_cipher::decode(std::string s) {
  std::string result;

  for (char const &c: s) {
    if ('a' <= c && c <= 'z') {
      result.push_back('a' + ('z' - c));
    } else if ('0' <= c && c <= '9') {
      result.push_back(c);
    }
  }

  return result;
}
