-module(raindrops).

-export([convert/1]).


-define(DROPS, [{3, "Pling"}, {5, "Plang"}, {7, "Plong"}]).


convert(Number) ->
  Sound = [Drop || {N, Drop} <- ?DROPS, Number rem N =:= 0],
  case Sound of
    [_|_] -> lists:flatten(Sound);
    [] -> integer_to_list(Number)
  end.
