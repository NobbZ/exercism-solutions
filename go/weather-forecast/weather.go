// Package weather is a package
package weather

var (
	// CurrentCondition is a variable
	CurrentCondition string
	// CurrentLocation is another variable
	CurrentLocation string
)

// Forecast is a function
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
