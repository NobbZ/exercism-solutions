module HelloWorld exposing (helloWorld)

import Maybe
import String


helloWorld : Maybe String -> String
helloWorld maybeName =
    maybeName
        |> Maybe.withDefault "World"
        |> \name -> String.concat [ "Hello, ", name, "!" ]
