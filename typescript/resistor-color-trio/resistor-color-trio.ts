type Color =
  | "black"
  | "brown"
  | "red"
  | "orange"
  | "yellow"
  | "green"
  | "blue"
  | "violet"
  | "grey"
  | "white";

type Prefix = "" | "kilo" |"mega"|"giga";

const colors = [
  "black",
  "brown",
  "red",
  "orange",
  "yellow",
  "green",
  "blue",
  "violet",
  "grey",
  "white",
];

function calcPrefix(n: number): { number: number; prefix: Prefix } {
  let prefixes: Array<Prefix> = ["kilo", "mega", "giga"];
  let prefix: Prefix | undefined = undefined;

  while (n >= 1000) {
    prefix = prefixes.shift();

    n = n / 1000;
  }

  return {
    number: n,
    prefix: prefix || "",
  };
}

export function decodedResistorValue(bands: Array<Color>): string {
  const [first, second, third] = bands;

  const ohms =
    (colors.indexOf(first) * 10 + colors.indexOf(second)) *
    Math.pow(10, colors.indexOf(third));

  const { number, prefix } = calcPrefix(ohms);

  return `${number} ${prefix}ohms`;
}
