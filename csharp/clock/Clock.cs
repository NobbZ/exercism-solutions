using System;

public struct Clock
{
    // CONSTANTS
    private const int MINUTES_PER_HOUR = 60;
    private const int HOURS_PER_DAY = 24;
    private const int MINUTES_PER_DAY =HOURS_PER_DAY * MINUTES_PER_HOUR;

    // PROPERTIES
    private int MinutesOfTheDay;

    public int Hours { get { return MinutesOfTheDay / MINUTES_PER_HOUR; } }

    public int Minutes { get { return MinutesOfTheDay % MINUTES_PER_HOUR; } }


    // CONSTRUCTORS
    public Clock(int hours, int minutes) : this(hours * MINUTES_PER_HOUR + minutes) {}

    private Clock(int minutes) {
        this.MinutesOfTheDay = modulo(minutes, MINUTES_PER_DAY);
    }

    // METHODS
    public Clock Add(int minutesToAdd) {
        return new Clock(this.MinutesOfTheDay + minutesToAdd);
    }

    public Clock Subtract(int minutesToSubtract) {
        return new Clock(this.MinutesOfTheDay - minutesToSubtract);
    }

    public override string ToString() {
        return String.Format("{0:00}:{1:00}", this.Hours, this.Minutes);
    }

    // STATIC HELPERS
    private static int modulo(int x, int m) {
        return (x % m + m) % m;
    }
}