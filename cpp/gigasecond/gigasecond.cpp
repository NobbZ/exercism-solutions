#include "gigasecond.h"

static const int GIGASECOND {1000000000};

static const boost::posix_time::time_duration GIGASECOND_AS_DURATION {boost::posix_time::seconds(GIGASECOND)};

boost::posix_time::ptime gigasecond::advance(boost::posix_time::ptime const &date) {
  return date + GIGASECOND_AS_DURATION;
}