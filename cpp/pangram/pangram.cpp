#include "pangram.h"

#include <bitset>
#include <cctype>

using charset = std::bitset<26>;

bool pangram::is_pangram(std::string str) {
    charset s;

    for (char c : str) {
        if (isalpha(c))
            s[tolower(c) - 'a'] = true;
        
        if (s.all())
            return true;
    }

    return false;
}