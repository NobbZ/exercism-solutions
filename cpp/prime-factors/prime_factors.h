#ifndef PRIME_FACTORS_PRIME_FACTORS_H
#define PRIME_FACTORS_PRIME_FACTORS_H

#include <vector>
#include <cstdint>

namespace prime_factors {
  std::vector<int> of(std::uintmax_t n);
}

#endif //PRIME_FACTORS_PRIME_FACTORS_H
