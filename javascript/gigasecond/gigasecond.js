// @ts-check

const SECOND = 1_000;
const KILOSECOND = 1_000 * SECOND;
const MEGASECOND = 1_000 * KILOSECOND;
const GIGASECOND = 1_000 * MEGASECOND;

/**
 * @param {Date} date
 * @returns {Date}
 */
export const gigasecond = (date) => {
  return new Date(date.getTime() + GIGASECOND);
};
