#ifndef RAINDROPS_RAINDROPS_H
#define RAINDROPS_RAINDROPS_H

#include <string>

namespace raindrops {
  std::string convert(int n);
}

#endif //RAINDROPS_RAINDROPS_H
