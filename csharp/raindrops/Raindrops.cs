using System.Collections.Generic;
using System;
using System.Linq;

public static class Raindrops
{
    private static IEnumerable<(int, string)> Drops = new List<(int, string)> {
        (3, "Pling"), (5, "Plang"), (7, "Plong"),
    };

    public static string Convert(int number)
    {
        return Drops
            .Where(p => number % p.Item1 == 0)
            .Select(p => p.Item2)
            .Aggregate("", (a, b) => a + b, (s) => string.Empty == s ? number.ToString() : s);
    }
}