#include "hamming.h"

#include <algorithm>
#include <stdexcept>
#include <vector>

template<class LeftOut, class RightOut, class LeftIt, class RightIt>
  std::vector<std::pair<LeftOut, RightOut>> zip(LeftIt  left_begin,  LeftIt  left_end,
                                                RightIt right_begin, RightIt right_end) {
    if (left_end - left_begin != right_end - right_begin) { throw std::domain_error {"Inequal length of input."}; }

    std::vector<std::pair<LeftOut, RightOut>> result;

    for (auto lit = left_begin, rit = right_begin;
         lit < left_end;
         ++lit, ++rit) {
      result.push_back(std::pair<LeftOut, RightOut> {*lit, *rit});
    }

    return result;
  }

unsigned int hamming::compute(std::string const& left, std::string const& right) {
  std::vector<std::pair<char, char>> pairs = zip<char, char>(std::cbegin(left), std::cend(left), std::cbegin(right), std::cend(right));

  return (unsigned int) std::count_if(std::cbegin(pairs), std::cend(pairs), [](std::pair<char, char> val) {
    return val.first != val.second;
  });
}