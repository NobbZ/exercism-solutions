#include "rna_transcription.h"

#include <algorithm>

char transcription::to_rna(char i) {
  switch (i) {
  case 'C': return 'G';
  case 'G': return 'C';
  case 'A': return 'U';
  case 'T': return 'A';
  }
}


std::string transcription::to_rna(std::string string){
  std::string result {string};
  std::transform(std::cbegin(string), std::cend(string), std::begin(result), [](char c) {
    return to_rna(c);
  });
  return result;
}
