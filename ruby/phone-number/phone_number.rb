module PhoneNumber
  def self.clean(number)
    result = number.chars
      .select { |d| d =~ /[0-9]/ }
      .join

    len = result.length

    return nil if invalid_lengths(result)

    result = result[-10..-1]

    return nil if invalid_area_code(result)
    return nil if invalid_exchange_code(result)

    result
  end

  def self.invalid_lengths(number)
    len = number.length
    
    len < 10 || (len == 11 && number[0] != '1') || len > 11
  end

  def self.invalid_area_code(number)
    %w[0 1].member? number[0]
  end

  def self.invalid_exchange_code(number)
    %w[0 1].member? number[3]
  end
end

module BookKeeping
  VERSION = 2
end