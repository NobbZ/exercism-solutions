class _Node<T extends Comparable<T>> {
  T data;

  _Node<T>? left;
  _Node<T>? right;

  _Node(this.data);

  void insert(T value) {
    var cmp = (value.compareTo(this.data));

    if (cmp <= 0) {
      this.left = _insert(this.left, value);
    } else {
      this.right = _insert(this.right, value);
    }
    ;
  }

  _Node<T> _insert(_Node<T>? node, T value) {
    if (node == null) return _Node<T>(value);

    node.insert(value);

    return node;
  }

  List<T> _sortedData() {
    return (this.left?._sortedData() ?? [])
        .followedBy([this.data])
        .followedBy(this.right?._sortedData() ?? [])
        .toList();
  }
}

class BinarySearchTree<T extends Comparable<T>> {
  _Node<T> root;

  BinarySearchTree(T rootValue) : root = _Node<T>(rootValue);

  void insert(T value) {
    this.root.insert(value);
  }

  List<T> get sortedData => this.root._sortedData();
}
