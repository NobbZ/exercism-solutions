#include "beer_song.h"

#include <vector>
#include <sstream>
#include <cstdio>

using std::string;

static const string ZERO_BEER =
  "No more bottles of beer on the wall, no more bottles of beer.\n"
  "Go to the store and buy some more, 99 bottles of beer on the wall.\n";

static const string ONE_BEER =
  "1 bottle of beer on the wall, 1 bottle of beer.\n"
  "Take it down and pass it around, no more bottles of beer on the wall.\n";

static const string TWO_BEER =
  "2 bottles of beer on the wall, 2 bottles of beer.\n"
  "Take one down and pass it around, 1 bottle of beer on the wall.\n";

static const string MORE_BEER_A =
  " bottles of beer on the wall, ";

static const string MORE_BEER_B =
  " bottles of beer.\nTake one down and pass it around, ";

static const string MORE_BEER_C =
  " bottles of beer on the wall.\n";

void verse_stream(int n, std::ostringstream &stream) {
  switch (n) {
  case 0: stream << ZERO_BEER; break;
  case 1: stream << ONE_BEER;  break;
  case 2: stream << TWO_BEER;  break;
  default: stream << std::to_string(n)     << MORE_BEER_A
                  << std::to_string(n)     << MORE_BEER_B
                  << std::to_string(n - 1) << MORE_BEER_C;
  }
}

string beer_song::verse(int n) {
  std::ostringstream stream;
  verse_stream(n, stream);
  return stream.str();
}

string beer_song::sing(int from, int to) {
  std::ostringstream stream;
  string prefix {""};

  for (int i = from; i >= to; --i) {
    stream << prefix << verse(i);
    prefix = "\n";
  }
  return stream.str();
}

string beer_song::sing(int from) {
  return sing(from, 0);
}
