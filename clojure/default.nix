{
  stdenv,
  lib,
  self,
  leiningen,
}: let
  runExercise = slug: sha256: let
    pnameSlug = builtins.replaceStrings ["-"] ["_"] slug;
    pname = "exercism_clojure_${pnameSlug}";
    version = "g${self.shortRev or "dirty"}";

    outputHash =
      if sha256 == null
      then lib.fakeSha256
      else sha256;

    deps = stdenv.mkDerivation {
      name = "deps-${pname}-${version}";

      src = lib.cleanSource (./. + "/${slug}");

      buildInputs = [leiningen];

      buildPhase = ''
        export HOME=$(mktemp -d)
        env
        lein deps
        ls -lah; exit 1
      '';

      outputHashAlgo = "sha256";
      outputHashMode = "flat";
      inherit outputHash;
    };
  in
    stdenv.mkDerivation {
      inherit pname version;

      src = lib.cleanSource (./. + "/${slug}");

      buildInputs = [leiningen deps];

      phases = ["unpackPhase" "checkPhase"];

      doCheck = true;
      checkPhase = ''
        export HOME=$(pwd)/home
        lein test | tee $out
      '';
    };
in {
  armstrong-nmbers = runExercise "armstrong-numbers" null;
  bob = runExercise "bob" null;
  hello-world = runExercise "hello-world" null;
  rna-transcription = runExercise "rna-transcription" null;
}
