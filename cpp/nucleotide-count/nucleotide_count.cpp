#include "nucleotide_count.h"

#include <algorithm>
#include <stdexcept>

bool is_valid_nucleotide(char nucleotide) {
  return 'A' == nucleotide
      || 'T' == nucleotide
      || 'C' == nucleotide
      || 'G' == nucleotide;
}

dna::counter::counter(std::string const strand) : strand(strand), the_count(count(strand)) { }

dna::nucleotide_count dna::counter::nucleotide_counts() const {
  return this->the_count;
}

int dna::counter::count(char nucleotide) const {
  if (!is_valid_nucleotide(nucleotide)) {
    throw std::invalid_argument {"The nucleotide '" + std::string{1, nucleotide} + "' is invalid!"};
  }
  return this->the_count.at(nucleotide); // can't use operator[] here, why?
}

dna::nucleotide_count dna::counter::count(std::string string) const {
  nucleotide_count result {{'A', 0},
                           {'T', 0},
                           {'C', 0},
                           {'G', 0}};
  std::for_each(string.cbegin(), string.cend(), [&result](char c) {
    if (!is_valid_nucleotide(c)) {
      throw std::invalid_argument {"The nucleotide '" + std::string{1, c} + "' is invalid!"};
    }
    ++result[c];
  });
  return result;
}
