#include "anagram.h"

#include <algorithm>

anagram::anagram::anagram(std::string subject) : subject(subject) { }

std::vector<std::string> anagram::anagram::matches(std::vector<std::string> const &tests) {
  if (!isNormalized()) { normalize(); }

  std::vector<std::string> result {};

  std::for_each(tests.cbegin(), tests.cend(), [&result, this](std::string const test) {
    const std::string normalized_test = normalize(test);
    if (   this->normalized == normalized_test
        && lowercase(test)  != lowercase(this->subject)) {
      result.push_back(test);
    }
  });

  return result;
}

bool anagram::anagram::isNormalized() {
  return this->normalized.empty() == this->subject.empty();
}

void anagram::anagram::normalize() {
  this->normalized = normalize(this->subject);
}

std::string anagram::anagram::normalize(std::string const &str) {
  std::string result = lowercase(str);
  std::sort(result.begin(), result.end());
  return result;
}

std::string anagram::anagram::lowercase(std::string const &str) {
  std::string result {str};
  std::transform(str.cbegin(), str.cend(), result.begin(), ::tolower);
  return result;
}