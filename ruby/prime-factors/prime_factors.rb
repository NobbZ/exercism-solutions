module PrimeFactors
  def self.for(n)
    result = Array.new
    m = 2

    while m <= n
      if n % m == 0
        result <<= m
        n /= m
      else
        m += 1
      end
    end

    result
  end
end