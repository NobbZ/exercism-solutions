{mkShell, writeShellScriptBin, sbcl, lispPackages}:

let testrunner = writeShellScriptBin "testrunner" ''
  name=$(basename $(pwd))

  filename=''${name}-test.lisp
  evalform="(''${name}-test:run-tests)"

  quicklisp run -- --non-interactive --load "''${filename}" --eval "''${evalform}"
'';
in
mkShell {
  packages = [sbcl lispPackages.quicklisp testrunner];
}
