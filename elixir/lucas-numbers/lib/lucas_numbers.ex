defmodule LucasNumbers do
  @moduledoc """
  Lucas numbers are an infinite sequence of numbers which build progressively
  which hold a strong correlation to the golden ratio (φ or ϕ)

  E.g.: 2, 1, 3, 4, 7, 11, 18, 29, ...
  """
  def generate(count) when not is_integer(count) or count <= 0,
    do: raise(ArgumentError, "count must be specified as an integer >= 1")

  def generate(1), do: [2]
  def generate(2), do: [2, 1]

  def generate(count) do
    heads = generate(2)

    tail =
      Stream.unfold(heads, fn [a, b] ->
        c = a + b
        {c, [b, c]}
      end)

    heads |> Stream.concat(tail) |> Enum.take(count)
  end
end
