defmodule BoutiqueInventory do
  def sort_by_price(inventory) do
    Enum.sort_by(inventory, & &1.price)
  end

  def with_missing_price(inventory) do
    Enum.filter(inventory, &(!&1.price))
  end

  def increase_quantity(item, count) do
    %{
      item
      | quantity_by_size:
          item.quantity_by_size
          |> Enum.map(fn {size, qty} -> {size, qty + count} end)
          |> Enum.into(%{})
    }
  end

  def total_quantity(item) do
    item.quantity_by_size
    |> Map.values()
    |> Enum.sum()
  end
end
