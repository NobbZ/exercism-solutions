#include "matching_brackets.h"

#include <stack>
#include <stdexcept>

namespace {
char closing(char c) {
  switch (c) {
  case '[': return ']';
  case '(': return ')';
  case '{': return '}';
  }

  throw std::domain_error("unexpected bracket");
}
} // namespace

bool matching_brackets::check(std::string const & s) {
  std::stack<char> brackets{};

  for (auto c : s) {
    switch (c) {
    case '(':
    case '[':
    case '{':
      brackets.push(closing(c));
      break;

    case ')':
    case ']':
    case '}':
      if (brackets.empty() || brackets.top() != c) {
        return false;
      }
      brackets.pop();
    }
  }

  return brackets.size() == 0;
}
