use std::{
    fmt::{self, Display, Formatter},
    ops::{Add, Rem},
};

#[derive(Debug, PartialEq, Eq)]
pub struct Clock(i32);

const HOURS_PER_DAY: i32 = 24;
const MINUTES_PER_HOUR: i32 = 60;
const MINUTES_PER_DAY: i32 = HOURS_PER_DAY * MINUTES_PER_HOUR;

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        (hours * MINUTES_PER_HOUR + minutes).into()
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        (self.0 + minutes).into()
    }
}

impl Display for Clock {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let m = self.0 % MINUTES_PER_HOUR;
        let h = self.0 / MINUTES_PER_HOUR;

        write!(f, "{:02}:{:02}", h, m)
    }
}

impl From<i32> for Clock {
    fn from(minutes: i32) -> Self {
        Clock(modulo(minutes, MINUTES_PER_DAY))
    }
}

fn modulo<T>(n: T, m: T) -> T
where
    T: Copy + Rem<Output = T> + Add<Output = T>,
{
    (n % m + m) % m
}
