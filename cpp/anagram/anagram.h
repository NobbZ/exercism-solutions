#ifndef ANAGRAM_ANAGRAM_H
#define ANAGRAM_ANAGRAM_H

#include <string>
#include <vector>

namespace anagram {
  class anagram {
  public:
    anagram(std::string subject);

    std::vector<std::string> matches(std::vector<std::string> const &tests);

  private:
    std::string subject;
    std::string normalized;

    bool isNormalized();

    void normalize();

    std::string normalize(std::string const &str);

    std::string lowercase(std::string const &str);
  };
}

#endif //ANAGRAM_ANAGRAM_H