-module(grains).

-export([square/1, total/0]).

-type square() :: 1..64.

-spec(square(square()) -> pos_integer()).
square(X) when is_integer(X)
               andalso X  >  0
               andalso X =< 64 ->
    1 bsl (X - 1). % Erlang has no power for integral, so I bitshift.

-spec(total() -> pos_integer()).
total() -> 1 bsl 64 - 1.