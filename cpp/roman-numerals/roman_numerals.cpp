#include "roman_numerals.h"

#include <sstream>
#include <vector>
#include <utility>

static const std::vector<std::pair<std::string, int>> ROMAN_MAP {
    {"M",  1000},
    {"CM", 900},
    {"D",  500},
    {"CD", 400},
    {"C",  100},
    {"XC", 90},
    {"L",  50},
    {"XL", 40},
    {"X",  10},
    {"IX", 9},
    {"V",  5},
    {"IV", 4},
    {"I",  1}
};

template<class InputIt>
  void convert_helper(std::ostringstream &stream, std::uintmax_t n, InputIt current, InputIt end) {
    if (current == end) { return; }

    std::uintmax_t times {n / current->second};
    std::uintmax_t remainder {n % current->second};

    for (std::uintmax_t i = times; i > 0; --i) {
      stream << current->first;
    }
    convert_helper(stream, remainder, ++current, end);
  }

std::string roman::convert(std::uintmax_t n) {
  std::ostringstream builder;
  convert_helper(builder, n, ROMAN_MAP.cbegin(), ROMAN_MAP.cend());
  return builder.str();
}
