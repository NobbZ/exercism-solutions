use std::cmp::Ordering;

use Comparison::*;

#[derive(PartialEq, Debug)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(v1: &[T], v2: &[T]) -> Comparison {
    match v1.len().cmp(&v2.len()) {
        Ordering::Equal   if v1 == v2           => Equal,
        Ordering::Less    if is_sublist(v1, v2) => Sublist,
        Ordering::Greater if is_sublist(v2, v1) => Superlist,
        _ => Unequal,
    }
}

fn is_sublist<T: PartialEq>(v1: &[T], v2: &[T]) -> bool {
    match (v1.len(), v2.len()) {
        (l1, l2) if l1 <= l2 => is_prefix(v1, v2) || is_sublist(v1, &v2[1..]),
        _ => false,
    }
}

fn is_prefix<T: PartialEq>(v1: &[T], v2: &[T]) -> bool {
    match (v1.len(), v2.len()) {
        (0, _) => true,
        (_, _) if v1[0] == v2[0] => is_prefix(&v1[1..], &v2[1..]),
        _ => false,
    }
}