#include "trinary.h"

#include <algorithm>
#include <numeric>
#include <stdexcept>

bool is_ternary_digit(char c) {
  return ('0' <= c) && (c <= '2');
}

bool valid_ternary_number(std::string const &subject) {
  return std::all_of(subject.begin(), subject.end(), [](char c) {
    return is_ternary_digit(c);
  });
}

unsigned int trinary::to_decimal(std::string const &tern_nmbr) {
  if (!valid_ternary_number(tern_nmbr)) { return 0; }

  return (unsigned int) std::accumulate(tern_nmbr.cbegin(), tern_nmbr.cend(), 0, [](unsigned int acc, char c) {
    // acc *= 3; // acc = acc << 1 + acc; which one is faster? How to benchmark properly?
    acc = (acc << 1) + acc; // Benchmarked it, see comment and bench.cpp
    acc += c - '0';
    return acc;
  });
}
