{
  callPackage,
  stdenv,
  self,
  lib,
  bats,
}: let
  runExercise = slug: let
    pname = builtins.replaceStrings ["-"] ["_"] slug;
  in
    stdenv.mkDerivation {
      pname = "exercism_bash_${pname}";
      version = "g${self.shortRev or "dirty"}";

      src = lib.cleanSource (./. + "/${slug}");

      buildInputs = [bats];

      phases = ["unpackPhase" "checkPhase"];

      doCheck = true;
      checkPhase = ''
        bats ${pname}_test.sh | tee $out
      '';
    };
in {
  armstrong-numbers = runExercise "armstrong-numbers";
  atbash-cipher = runExercise "atbash-cipher";
  bob = runExercise "bob";
  collatz-conjecture = runExercise "collatz-conjecture";
  error-handling = runExercise "error-handling";
  gigasecond = runExercise "gigasecond";
  grains = runExercise "grains";
  hamming = runExercise "hamming";
  hello-world = runExercise "hello-world";
  leap = runExercise "leap";
  luhn = runExercise "luhn";
  nucleotide-count = runExercise "nucleotide-count";
  pangram = runExercise "pangram";
  raindrops = runExercise "raindrops";
  two-fer = runExercise "two-fer";
}
