#pragma once

#include <string>

namespace two_fer {
  std::string two_fer(std::string const & s = "you");
} // namespace two_fer
