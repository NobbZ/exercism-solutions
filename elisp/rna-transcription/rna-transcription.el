;;; rna-transcription.el -- RNA Transcription (exercism)

;;; Commentary:

;;; Code:

(defun to-rna (dna)
  "Convert given DNA to RNA."
  (let ((conversion-plist '(?A ?U ?C ?G ?G ?C ?T ?A))
        (convert (lambda (nucleotide)
                   (plist-get conversion-plist nucleotide))))
    (apply #'string (mapcar convert dna))))

(provide 'rna-transcription)
;;; rna-transcription.el ends here
