#include "reverse_string.h"

#include <algorithm>

std::string reverse_string::reverse_string(std::string const & s) {
  return std::string(s.crbegin(), s.crend());
}
