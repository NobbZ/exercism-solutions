#!/usr/bin/env bash

set -o errexit
set -o nounset

print_total () {
    printf "%u\n" $(( 0xffffffffffffffff ))
}

print_score () {
    printf "%u\n" $(( 1 << ($1 - 1) ))
}

main () {
    local n=${1}

    if [[ $n == "total" ]]; then
        print_total
    elif (( 1 <= $n & $n <= 64 )); then
        print_score $n
    else
        printf "Error: invalid input\n"
        return 1
    fi
}

main "$@"
