-module(hamming).

-export([distance/2]).

distance(Dna1, Dna2) when is_list(Dna1) and is_list(Dna2) ->
    distance(Dna1, Dna2, 0).

distance([], [], Acc)                 -> Acc;
distance([A | Dna1], [A | Dna2], Acc) -> distance(Dna1, Dna2, Acc);
distance([_ | Dna1], [_ | Dna2], Acc) -> distance(Dna1, Dna2, Acc + 1);
distance(_, _, _)                     -> {error, badarg}.
