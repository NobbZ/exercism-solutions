#ifndef NTH_PRIME_NTH_PRIME_H
#define NTH_PRIME_NTH_PRIME_H

#include <cstdint>

namespace prime {
  std::uintmax_t nth(std::intmax_t n);
}

#endif //NTH_PRIME_NTH_PRIME_H
