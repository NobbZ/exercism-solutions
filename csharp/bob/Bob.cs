using System;
class Bob {
  private enum Classification {
    FORCED_QUESTION,
    SHOUT,
    SILENCE,
    QUESTION,
    OTHER
  }

  private static Classification Classify(string sentence) {
    bool silence = true;
    bool shout = true;
    bool question = true;
    bool thereWereLetters = false;

    foreach (char c in sentence) {
      if (char.IsLetter(c) && char.IsLower(c)) {
        shout = false;
      } 
      if (c == '?') {
        question = true;
      } else if (!char.IsWhiteSpace(c)) {
        silence = false;
        question = false;
      }
      thereWereLetters = thereWereLetters || char.IsLetter(c);
    }

    if (silence) { 
      return Classification.SILENCE; 
    } else if (shout && thereWereLetters && question) {
      return Classification.FORCED_QUESTION;
    } else if (shout && thereWereLetters) {
      return Classification.SHOUT; 
    } else if (question) { 
      return Classification.QUESTION; 
    } else { 
      return Classification.OTHER;
    }
  }

  internal static string Response(string sentence) {
    switch (Classify(sentence.Trim())) {
    case Classification.SHOUT:
      return "Whoa, chill out!";
    case Classification.QUESTION:
      return "Sure.";
    case Classification.SILENCE:
      return "Fine. Be that way!";
    case Classification.OTHER:
      return "Whatever.";
    case Classification.FORCED_QUESTION:
      return "Calm down, I know what I'm doing!";
    default:
      throw new Exception("Unreachable");
    }
  }
}