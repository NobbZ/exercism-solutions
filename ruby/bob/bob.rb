class Sentence < String
  def initialize(str)
    super(str.strip)
  end

  def silence?
    @silence ||= self.empty?
  end

  def shout?
    @norm ||= gsub(/[^[:alpha:]]/, '')
    @shout ||= !@norm.empty? && @norm.upcase == @norm
  end

  def question?
    @question ||= self.end_with? '?'
  end
end

class Bob
  def self.hey(str)
    snt = Sentence.new str
    res ||= ('Calm down, I know what I\'m doing!' if snt.shout? && snt.question?)
    res ||= ('Fine. Be that way!'                 if snt.silence?)
    res ||= ('Whoa, chill out!'                   if snt.shout?)
    res ||= ('Sure.'                              if snt.question?)
    res || 'Whatever.'
  end
end

class BookKeeping
  VERSION = 2
end