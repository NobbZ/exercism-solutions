module Binary
  def self.to_decimal(str)
    str.chars.reduce(0) do |acc, digit|
      raise ArgumentError unless %w(0 1).member? digit
      (acc << 1) | digit.ord - '0'.ord
    end
  end
end

module BookKeeping
  VERSION = 3
end