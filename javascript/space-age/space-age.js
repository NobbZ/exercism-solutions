// @ts-check

/** @typedef {"mercury"|"venus"|"earth"|"mars"|"jupiter"|"saturn"|"uranus"|"neptune"} Planet */

/** @type {Record<Planet, number>} */
const multiplier = {
  earth:     1.0,
  mercury:   0.2408467,
  venus:     0.61519726,
  mars:      1.8808158,
  jupiter:  11.862615,
  saturn:   29.447498,
  uranus:   84.016846,
  neptune: 164.79132,
};

/** @type {number} */
const earthYearDuration = 31557600.0;

/**
 * @param {Planet} planet
 * @param {number} secondsOnEarth
 */
export const age = (planet, secondsOnEarth) => {
  const exact = secondsOnEarth / (earthYearDuration * multiplier[planet]);
  const rounded = exact.toFixed(2);
  return Number(rounded);
};
