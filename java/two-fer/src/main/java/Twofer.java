import java.util.Optional;

public class Twofer {
    private static String NAME = "you";

    public String twofer(String optionalName) {
        String name = Optional.ofNullable(optionalName).orElse(NAME);

        return String.format("One for %s, one for me.", name);
    }
}
