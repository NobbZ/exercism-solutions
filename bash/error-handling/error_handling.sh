#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
    local name=${1:-first_argument_unbound}

    if (( $# == 0 )); then
        printf "Usage: ./error_handling <greetee>\n"
        return 1
    elif (( $# >= 2 )); then
        return $#
    fi

    printf "Hello, %s\n" "${name}"
}

main "$@"
