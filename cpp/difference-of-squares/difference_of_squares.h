#ifndef DIFFERENCE_OF_SQUARES_DIFFERENCE_OF_SQUARES_H
#define DIFFERENCE_OF_SQUARES_DIFFERENCE_OF_SQUARES_H

namespace squares {
  unsigned int square_of_sums(int const n);

  unsigned int sum_of_squares(int const n);

  unsigned int difference(int const n);
}

#endif //DIFFERENCE_OF_SQUARES_DIFFERENCE_OF_SQUARES_H
