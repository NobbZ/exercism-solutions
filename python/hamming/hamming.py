def distance(l, r):
    if len(l) != len(r):
        raise ValueError("s")

    return sum(a != b for a, b in zip(l, r))
