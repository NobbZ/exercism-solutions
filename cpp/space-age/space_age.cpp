#include "space_age.h"

#define ONPLANET(F, P) space_age::year space_age::space_age::F() const { \
  return calc_age_on_planet(this->base_value, P); \
}

static const double EARTH_YEAR_DURATION = 31557600.0;

// Don't touch planet or this won't work!
static const double PLANET_MODIFIERS[] {
    1.0, 0.2408467, 0.61519726, 1.8808158, 11.862615, 29.447498, 84.016846, 164.79132
};

// Don't touch, or PLANET_MODIFIERS wont work!
enum planet {
  EARTH,
  MERCURY,
  VENUS,
  MARS,
  JUPITER,
  SATURN,
  URANUS,
  NEPTUNE
};


space_age::year calc_age_on_planet(space_age::second seconds, ::planet planet) {
  return (double) seconds / (EARTH_YEAR_DURATION * PLANET_MODIFIERS[planet]);
}

space_age::space_age::space_age(second i) : base_value(i) { }

space_age::second space_age::space_age::seconds() const {
  return this->base_value;
}

ONPLANET(on_earth, EARTH)
ONPLANET(on_mercury, MERCURY)
ONPLANET(on_neptune, NEPTUNE)
ONPLANET(on_uranus, URANUS)
ONPLANET(on_saturn, SATURN)
ONPLANET(on_jupiter, JUPITER)
ONPLANET(on_mars, MARS)
ONPLANET(on_venus, VENUS)