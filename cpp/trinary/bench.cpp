#include <hayai/hayai.hpp>

#define RUNS 1000
#define ITERATIONS 10000000

// @formatter:off
BENCHMARK(times_3, shift_and_add, RUNS, ITERATIONS) {
  (100 << 1) + 100;
}

BENCHMARK(times_3, using_times, RUNS, ITERATIONS) {
  100 * 3;
}
// @formatter:on

int main() {
  hayai::ConsoleOutputter co;

  hayai::Benchmarker::AddOutputter(co);
  hayai::Benchmarker::RunAllTests();

  return 0;
}

// Original iteration:
// http://exercism.io/submissions/30874ee81e404dfaa10941bd210d75d7