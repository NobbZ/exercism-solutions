#lang racket

(provide (contract-out
          [leap-year? (-> number? boolean?)]))

(define (leap-year? year)
  (or (divides 400 year)
      (and (divides 4 year)
           (not (divides 100 year)))))

(define (divides n m)
  (= (modulo m n) 0))

