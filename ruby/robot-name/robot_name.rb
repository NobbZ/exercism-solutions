class Robot
  attr_reader :name

  def self.forget
    @@next = 'AA000'
  end

  def initialize
    reset
  end

  def reset
    @name = @@next
    @@next = @@next.succ
  end
end

module BookKeeping
  VERSION = 3
end