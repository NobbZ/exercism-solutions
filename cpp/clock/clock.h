#ifndef CLOCK_CLOCK_H
#define CLOCK_CLOCK_H

#include <string>

namespace date_independent {
  class clock {
  public:
    static clock at(int, int = 0);

    clock plus(int);

    clock minus(int);

    operator std::string () const;
    bool operator==(clock) const;
    bool operator!=(clock) const;

  private:
    const int _minutes;

    clock(int);
    clock(int, int);
  };
}

#endif //CLOCK_CLOCK_H
