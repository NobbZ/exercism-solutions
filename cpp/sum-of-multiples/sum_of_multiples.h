#ifndef SUM_OF_MULTIPLES_SUM_OF_MULTIPLES_H
#define SUM_OF_MULTIPLES_SUM_OF_MULTIPLES_H

#include <vector>

namespace sum_of_multiples {
  typedef std::vector<int> divisor_list;

  int to(divisor_list divisors, int max);
}

#endif //SUM_OF_MULTIPLES_SUM_OF_MULTIPLES_H
