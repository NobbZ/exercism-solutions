#include "hexadecimal.h"

std::size_t hexadecimal::convert(std::string const & hex) {
  std::size_t result {0};

  for (auto digit : hex) {
    result <<= 4;

    if ('0' <= digit && digit <= '9') {
      result |= digit - '0';
    } else if ('a' <= digit && digit <= 'f') {
      result |= digit - 'a' + 10;
    } else if ('A' <= digit && digit <= 'F') {
      result |= digit - 'A' + 10;
    } else {
      return 0;
    }
  }

  return result;
}
