#include "grains.h"

static const grains::grain_count ONE = static_cast<grains::grain_count>(1);

grains::grain_count grains::square(grains::square_number number) {
  // Fastes way to perform $2^{number - 1}$ in C and C++ (perhaps most other languages too)
  return ONE << (number - ONE);
}

grains::grain_count grains::total() {
  return UINT64_MAX; // std::uint64_t is a perfect copy of the chessboard!
}
