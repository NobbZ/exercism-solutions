#include "prime_factors.h"

std::vector<int> prime_factors::of(std::uintmax_t n) {
  std::vector<int> result {};
  std::uintmax_t   m      {2};

  while (m <= n) {
    if (!(n % m)) {
      result.push_back(m);
      n = n / m;
    } else {
      ++m;
    }
  }

  return result;
}