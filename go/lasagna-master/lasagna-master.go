package lasagna

func PreparationTime(layers []string, minutesPerLayer int) int {
	if minutesPerLayer == 0 {
		minutesPerLayer = 2
	}

	return len(layers) * minutesPerLayer
}

func Quantities(layers []string) (noodles int, sauce float64) {
	for _, layer := range layers {
		switch layer {
		case "sauce":
			sauce += 0.2
		case "noodles":
			noodles += 50
		}
	}
	return
}

func AddSecretIngredient(friendsList, myList []string) []string {
	var result = make([]string, 0)
	result = append(result, myList...)
	return append(result, friendsList[len(friendsList)-1])
}

func ScaleRecipe(quantities []float64, scale int) []float64 {
	var result = make([]float64, len(quantities))

	for idx, v := range quantities {
		result[idx] = v / 2 * float64(scale)
	}

	return result
}
