#ifndef ETL_ETL_H
#define ETL_ETL_H

#include <map>
#include <vector>

namespace etl {
  typedef std::map<int, std::vector<char>> old_format;
  typedef std::map<char, int>              new_format;

  new_format transform(old_format const &);
}

#endif //ETL_ETL_H
