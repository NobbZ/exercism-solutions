module WordCount (wordCount) where

import qualified Data.Char       as Char
import qualified Data.List       as List
import           Data.Map.Strict (Map, empty, insertWith)

wordCount :: String -> Map String Int
wordCount text = List.foldl' buildmap empty normWords
  where
    buildmap xs x = insertWith (+) x 1 xs
    normWords = map (map Char.toLower) plainWords
    plainWords = tokenize text ""
    tokenize "" "" = []
    tokenize "" token = [reverse token]
    tokenize (a:'\'':c:cs) token | Char.isAlphaNum a && Char.isAlphaNum c = tokenize cs $ c:'\'':a:token
    tokenize (c:cs) token | Char.isAlphaNum c = tokenize cs $ c:token
    tokenize (_:cs) "" = tokenize cs ""
    tokenize (_:cs) token = reverse token:tokenize cs ""
